package my.project.nativeimageprocessing;

import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ducdang on 2/21/17.
 */

public class heartRateEstimationThread  extends HandlerThread {
    private static final String TAG = heartRateEstimationThread.class.getSimpleName();
    private Handler mWorkerHandler;
    private Handler mRespondHandler;
    private List<Double> heartRateData;
    private heartRateCallback mCallBack;


    public interface heartRateCallback {
        void onNewHeartRateValue(double newHeartRateValue);
    }

    public heartRateEstimationThread(Handler respondHandler, heartRateEstimationThread.heartRateCallback callBack) {
        super(TAG);
        heartRateData = new CopyOnWriteArrayList<>();
        mRespondHandler = respondHandler;
        mCallBack = callBack;
    }

    public void queueTask(int what, double[] ppgData) {
        //count = 0;
        Log.i(TAG, "New Data section added");
        mWorkerHandler.obtainMessage(what, ppgData).sendToTarget();
    }

    public void prepareHandler() {
        mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                //count = msg.what;
                double[] ppgData = (double[]) msg.obj;

                //final double heartRate = nativeHeartRateCalculation(ppgData);
                final double heartRate = 0;
                Log.d(TAG, "New Data Section Size: " + ppgData.length );
                Log.d(TAG, "Heartrate: " + heartRate );

                mRespondHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mCallBack.onNewHeartRateValue(heartRate);
                    }
                });
                return true;
            }
        });
    }

    private void toHeartRateDataFile(){
        try {
            String dataFileName = "heartRate.txt";
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            String MyDirectory_path = extStorageDirectory + "/" + "MyCamera";
            final String dataPath = MyDirectory_path + "/" + dataFileName;

            File dataFolder = new File(MyDirectory_path);
            if (!dataFolder.isDirectory() && !dataFolder.mkdirs()) {
                Log.e(TAG, "Failed to create app data directory at " + MyDirectory_path);
            }
            File dataFile = new File(dataPath);
            FileWriter writer = new FileWriter(dataFile);
            Iterator<Double> iterator = heartRateData.iterator();
            while (iterator.hasNext()) {
                Double avg = iterator.next();
                writer.append(avg.toString());
                writer.write("\n");
            }
            Log.d(TAG, "Data stored at: " + MyDirectory_path);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public native double nativeHeartRateCalculation(double[] data);

}
