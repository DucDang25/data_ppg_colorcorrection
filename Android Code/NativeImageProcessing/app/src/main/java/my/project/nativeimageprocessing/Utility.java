package my.project.nativeimageprocessing;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import org.opencv.core.Mat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.StringTokenizer;

import static android.content.ContentValues.TAG;
import static org.opencv.core.CvType.*;

/**
 * Created by ducdang on 4/12/17.
 */

public class Utility {

    public static double[][] convertingMat = {{9.5934, 11.4168, 11.4969},
                                              {0.6799, -0.3709, -0.2026},
                                              {0.2469, 1.0121,  0.0238},
                                              {0.0746, 0.3203,  1.0801}};

    public static void saveCorrectionMatrix (Context context, Mat matrix){
        int matWidth  = context.getResources().getInteger(R.integer.correction_matrix_width);
        int matHeight = context.getResources().getInteger(R.integer.correction_matrix_height);

        double matrixBuffer[] = new double[(int)matrix.total()];
        matrix.get(0,0,matrixBuffer);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();

        StringBuilder str = new StringBuilder();
        for (int i = 0; i < matHeight; i++) {
            for (int j = 0; j < matWidth; j ++){
                str.append(matrixBuffer[i*matWidth + j]);
                if(j < (matWidth-1)){
                    str.append(",");
                }else if (i < (matHeight -1)){
                    str.append(";");
                }
            }
        }
        editor.putString(context.getString(R.string.pref_color_correction_matrix_key), str.toString());
        editor.apply();
    }

    public static Mat loadCorrectionMatrix (Context context){
        int matWidth  = context.getResources().getInteger(R.integer.correction_matrix_width);
        int matHeight = context.getResources().getInteger(R.integer.correction_matrix_height);

        Mat matrix = new Mat(4,3,CV_64FC1);
        double matrixBuffer[] = new double[(int)matrix.total()];

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String matrixString = prefs.getString(
                context.getString(R.string.pref_color_correction_matrix_key),
                context.getResources().getString(R.string.pref_color_correction_matrix_default));

        StringTokenizer semicolonTokenizer = new StringTokenizer(matrixString, ";");
        for (int i = 0; i < matHeight; i++) {
            String rowString = semicolonTokenizer.nextToken();
            StringTokenizer commaTokenizer = new StringTokenizer(rowString, ",");
            for (int j = 0; j < matWidth; j++){
                matrixBuffer[i*matWidth + j] = Double.parseDouble(commaTokenizer.nextToken());
            }
        }
        matrix.put(0,0,matrixBuffer);
        return matrix;
    }

    public static void resetDefaultCorrectionMatrix(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(context.getString(R.string.pref_color_correction_matrix_key),
                context.getResources().getString(R.string.pref_color_correction_matrix_default));
        editor.apply();
    }

    public static void bitmapToFile(Bitmap bitmap, String fileName){
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        String MyDirectory_path = extStorageDirectory + "/PPGapp/";
        final String photoPath      = MyDirectory_path + "/" + fileName;

        File album = new File(MyDirectory_path);
        if (!album.isDirectory() && !album.mkdirs()) {
            Log.e(TAG, "Failed to create album directory at " + MyDirectory_path);
            return;
        }
        OutputStream outputStream;

        try {
            outputStream = new FileOutputStream(photoPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        Log.i(TAG, "Image saved at" + MyDirectory_path);
    }

}
