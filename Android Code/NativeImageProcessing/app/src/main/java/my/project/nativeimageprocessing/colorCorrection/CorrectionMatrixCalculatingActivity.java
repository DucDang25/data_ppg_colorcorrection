package my.project.nativeimageprocessing.colorCorrection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import java.io.File;
import my.project.nativeimageprocessing.R;
import my.project.nativeimageprocessing.Utility;

public class CorrectionMatrixCalculatingActivity extends AppCompatActivity {

    CalculateCorrectionMatrixTask corrMatCalTask;
    ProgressBar calculateCorrectionMatrixProgressBar;
    ImageView refCheckerImageView;
    ImageView capturedCheckerImageView;
    Button calculateCorrMatButton;
    Button showCorrMatButton;
    Button resetDefaultCorrMatButton;
    TextView correctionMatrixTextView;
    String generatedCheckerPhotoPath;
    private static final String TAG = "CalculateCorrectionMat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correction_matrix_calculating);
        refCheckerImageView = (ImageView) findViewById(R.id.imageView_reference_color_checker);
        capturedCheckerImageView = (ImageView) findViewById(R.id.imageView_captured_color_checker);
        correctionMatrixTextView = (TextView) findViewById(R.id.text_view_correction_matrix);

        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        generatedCheckerPhotoPath      = extStorageDirectory + "/PPGapp/" + "generatedChecker.png";



        calculateCorrectionMatrixProgressBar = (ProgressBar) findViewById(R.id.calculate_correction_matrix_progress_bar);
        calculateCorrMatButton = (Button) findViewById(R.id.button_calculateCorrectionMatrix);
        calculateCorrMatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                corrMatCalTask = new CalculateCorrectionMatrixTask(CorrectionMatrixCalculatingActivity.this);
                corrMatCalTask.execute();
            }
        });

        showCorrMatButton = (Button) findViewById(R.id.button_showCorrectionMatrix);
        showCorrMatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mat testCorrectionMatrix = Utility.loadCorrectionMatrix(CorrectionMatrixCalculatingActivity.this);
                correctionMatrixTextView.setText(testCorrectionMatrix.dump());
            }
        });

        resetDefaultCorrMatButton = (Button) findViewById(R.id.button_resetDefaultCorrectionMatrix);
        resetDefaultCorrMatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.resetDefaultCorrectionMatrix(CorrectionMatrixCalculatingActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Picasso.with(this)
                .load(R.drawable.chart_classic)
                .error(R.drawable.image_not_available)
                .into(refCheckerImageView);

        Picasso.with(this)
                .load(new File(generatedCheckerPhotoPath))
                .error(R.drawable.image_not_available)
                .into(capturedCheckerImageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(corrMatCalTask != null){
            corrMatCalTask.setActivity(null);
            corrMatCalTask.cancel(true);
        }
    }

    private static class CalculateCorrectionMatrixTask extends AsyncTask<Void, Void, Void> {
        private CorrectionMatrixCalculatingActivity corrMatActicity;
        private Mat imMatReference = new Mat();
        private Mat imMatCaptured = new Mat();
        private Mat imMatCorrected = new Mat();
        private Mat imMatGenerated = new Mat();
        private Mat correctionMat  = new Mat();

        public CalculateCorrectionMatrixTask(CorrectionMatrixCalculatingActivity activity){
            corrMatActicity = activity;
        }

        public void setActivity(CorrectionMatrixCalculatingActivity activity){
            corrMatActicity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            corrMatActicity.calculateCorrectionMatrixProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            getCorrectionMatrix();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // Save Calculated correction matrix to Shared Preference
            Utility.saveCorrectionMatrix(corrMatActicity,correctionMat);
            Mat testCorrectionMatrix = Utility.loadCorrectionMatrix(corrMatActicity);
            Log.d(TAG,"Test saved Correction Matrix: " + testCorrectionMatrix.dump());

            // Load Newly Calculated correction matrix from Shared Preference and display it
            corrMatActicity.correctionMatrixTextView.setText(testCorrectionMatrix.dump());
            corrMatActicity.calculateCorrectionMatrixProgressBar.setVisibility(View.GONE);
            Picasso.with(corrMatActicity)
                    .load(R.drawable.chart_classic)
                    .error(R.drawable.image_not_available)
                    .into(corrMatActicity.refCheckerImageView);

            Picasso.with(corrMatActicity)
                    .load(new File(corrMatActicity.generatedCheckerPhotoPath))
                    .error(R.drawable.image_not_available)
                    .into(corrMatActicity.capturedCheckerImageView);
        }

        public void getCorrectionMatrix(){
            String photoPathReference = Environment.getExternalStorageDirectory()+"/PPGapp/" + "ref.png";
            String photoPathCaptured = Environment.getExternalStorageDirectory()+"/PPGapp/" + "capturedChecker.png";

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap capturedCheckerBitmap = BitmapFactory.decodeFile(photoPathCaptured, options);
            Utils.bitmapToMat(capturedCheckerBitmap,imMatCaptured);

            Bitmap referenceCheckerBitmap = BitmapFactory.decodeResource(corrMatActicity.getResources(), R.drawable.chart_classic, options);
            Utils.bitmapToMat(referenceCheckerBitmap,imMatReference);

            Utility.bitmapToFile(referenceCheckerBitmap, "testRefImage.png");

            Log.d(TAG,"Reference Bitmap image size: " + referenceCheckerBitmap.getWidth() + "x" + referenceCheckerBitmap.getHeight());
            Log.d(TAG,"Generated image size: " + imMatCaptured.size().width + "x" + imMatCaptured.size().height);
            Log.d(TAG,"Reference image size: " + imMatReference.size().width + "x" + imMatReference.size().height);
            Log.d(TAG,"Reference image type: " + imMatReference.type());
            Log.d(TAG,"Generated image type: " + imMatCaptured.type());

            corrMatActicity.getCorrectionMatrix(imMatReference.getNativeObjAddr(),
                                                imMatCaptured.getNativeObjAddr(),
                                                imMatGenerated.getNativeObjAddr(),
                                                correctionMat.getNativeObjAddr(),
                                                imMatCorrected.getNativeObjAddr());
            Log.d(TAG,"Generated chart size: " + correctionMat.size().width + "x" + correctionMat.size().height);
            Log.d(TAG,"Generated chart dimentions: " + correctionMat.dims());
            Log.d(TAG,"Generated Correction Matrix: " + correctionMat.dump());

            Bitmap generatedCheckerBitmap = Bitmap.createBitmap((int)imMatGenerated.size().width, (int) imMatGenerated.size().height, options.inPreferredConfig);
            Utils.matToBitmap(imMatGenerated,generatedCheckerBitmap);
            Utility.bitmapToFile(generatedCheckerBitmap, "generatedChecker.png");
            Log.d(TAG,"Converting Matrix size: " + correctionMat.size().width + " x " + correctionMat.size().height);

        }
    }

    public native void getCorrectionMatrix(long matAddrRef, long matAddrCaptured, long matAddrGeneratedChecker, long matAddrConvertion, long matAddrCorrected);

}
