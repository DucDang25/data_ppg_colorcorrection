package my.project.nativeimageprocessing;
import com.androidplot.xy.XYSeries;

/**
 * Created by ducdang on 10/24/16.
 * Modified from ECG example, AndroidPlot Library
 */

public class PPGSeries implements XYSeries {
    private final Number[] data;
    private int latestIndex;

    public PPGSeries(int size) {
        //this.listener = listener;
        data = new Number[size];
        for (int i = 0; i < data.length; i++) {
            data[i] = 0;
        }
    }

    public void addSample(double value){
        if (latestIndex >= data.length) {
            latestIndex = 0;
        }

        data[latestIndex] = value;
        if(latestIndex < data.length - 1) {
            // null out the point immediately following i, to disable
            // connecting i and i+1 with a line:
            data[latestIndex +1] = null;
        }
        latestIndex++;
    }

    @Override
    public int size() {
        return data.length;
    }
    @Override
    public Number getX(int index) {
        return index;
    }
    @Override
    public Number getY(int index) {
        return data[index];
    }
    @Override
    public String getTitle() {
        return "PPSignal";
    }
    public int getLatestIndex(){ return latestIndex;}
}
