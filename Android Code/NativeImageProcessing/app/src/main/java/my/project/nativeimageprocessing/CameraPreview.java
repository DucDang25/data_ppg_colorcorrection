package my.project.nativeimageprocessing;

/**
 * Created by ducdang on 10/24/16.
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.TextView;
import android.widget.Toast;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.AdvancedLineAndPointRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.XYPlot;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import my.project.nativeimageprocessing.colorCorrection.ImageCapturingActivity;

import static android.content.ContentValues.TAG;
import static android.hardware.Camera.Parameters.FOCUS_MODE_FIXED;
import static android.hardware.Camera.Parameters.FOCUS_MODE_MACRO;

@SuppressWarnings("deprecation")
public class CameraPreview implements SurfaceHolder.Callback,
        Camera.PreviewCallback, ImageProcessingThread.Callback,
        heartRateEstimationThread.heartRateCallback, spo2EstimationThread.spo2Callback{
    private Camera mCamera = null;
    private int PreviewSizeWidth;
    private int PreviewSizeHeight;
    private static final String TAG = "CameraPreview";
    private int count = 0;
    public boolean isStopped = false;
    private ImageProcessingThread mWorkerThread;
    private heartRateEstimationThread heartRateThread;
    private spo2EstimationThread spo2Thread;

    private PPGSeries ppgSeries;
    private XYPlot ppgPlot;
    private Redrawer redrawer;
    private List<Double> ppgListGreen ;
    private List<Double> ppgListRed ;
    private TextView hr_textview;
    private TextView spo2_textview;
    private Activity activity;

    public CameraPreview(Activity activity, int PreviewlayoutWidth, int PreviewlayoutHeight) {
        this.activity = activity;
        PreviewSizeWidth = PreviewlayoutWidth;
        PreviewSizeHeight = PreviewlayoutHeight;
        if(this.activity instanceof MyCamera){
            if (ppgPlot == null){
                initPlot(activity);
            }
            hr_textview = (TextView) activity.findViewById(R.id.hr_textview);
            spo2_textview = (TextView) activity.findViewById(R.id.spo2_textview);
            redrawer = new Redrawer(ppgPlot, 30, true);
        }
    }

    private void initPlot(Activity activity){
        ppgPlot = (XYPlot) activity.findViewById(R.id.ppg_plot);
        int displayDuration = 3; // display duration in second(s)
        ppgSeries = new PPGSeries(displayDuration*30);
        ppgPlot.addSeries(ppgSeries, new MyFadeFormatter(displayDuration*30));
        ppgPlot.setRangeBoundaries(0, 255, BoundaryMode.AUTO);
        ppgPlot.setDomainBoundaries(0, displayDuration*30, BoundaryMode.FIXED);
        // reduce the number of range labels
        ppgPlot.setLinesPerRangeLabel(3);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera mCamera)
    {
        if(this.activity instanceof MyCamera){
            if (!isStopped){
                mWorkerThread.queueTask(count, PreviewSizeWidth, PreviewSizeHeight, data);
                Log.d(TAG, "Current time: "  + System.currentTimeMillis());
                Log.d(TAG, "Width: " + PreviewSizeWidth );
                Log.d(TAG, "Height: " + PreviewSizeHeight );
            }
        }else if(this.activity instanceof ImageCapturingActivity){

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        if(this.activity instanceof MyCamera){
            mWorkerThread = new ImageProcessingThread(this.activity ,new Handler(), this);
            mWorkerThread.start();
            mWorkerThread.prepareHandler();
            ppgListGreen = new ArrayList<>();
            ppgListRed = new ArrayList<>();
            heartRateThread = new heartRateEstimationThread(new Handler(), this);
            heartRateThread.start();
            heartRateThread.prepareHandler();
            spo2Thread = new spo2EstimationThread(new Handler(), this);
            spo2Thread.start();
            spo2Thread.prepareHandler();
        }

        Camera.Parameters parameters;
        parameters = mCamera.getParameters();
        String flat = parameters.flatten();
        parameters.setPreviewSize(PreviewSizeWidth, PreviewSizeHeight);
//        parameters.setPictureSize(PreviewSizeWidth, PreviewSizeHeight);
        parameters.setPreviewFrameRate(activity.getResources().getInteger(R.integer.fps));
        parameters.setPreviewFpsRange(activity.getResources().getInteger(R.integer.fps_range), activity.getResources().getInteger(R.integer.fps_range));
        //parameters.setExposureCompensation(parameters.getMinExposureCompensation());
        parameters.setExposureCompensation(0);

        String NowFlashMode = parameters.getFlashMode();
        if ( NowFlashMode != null )
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);

        String NowFocusMode = parameters.getFocusMode ();
        String[] focusModes = null;
        String focus_keyword=null;
        if ( NowFocusMode != null ) {
            if(flat.contains("focus-mode-values")){
                focus_keyword="focus-mode-values";
                //focus_modes_keyword="focus-mode";
                String focus_modes = flat.substring(flat.indexOf(focus_keyword));
                focus_modes = focus_modes.substring(focus_modes.indexOf("=")+1);

                if(focus_modes.contains(";"))
                        focus_modes = focus_modes.substring(0, focus_modes.indexOf(";"));

                focusModes = focus_modes.split(",");

                if (Arrays.asList(focusModes).contains("macro")){
                    parameters.setFocusMode(FOCUS_MODE_MACRO);
                    Log.i(TAG, "Focus: "  + parameters.getFocusMode());
                }else{
                    parameters.setFocusMode(FOCUS_MODE_FIXED);
                    Log.i(TAG, "Focus: "  + parameters.getFocusMode());
                }
            }
        }

        mCamera.setParameters(parameters);
        mCamera.setDisplayOrientation(90);
        mCamera.startPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        mCamera = Camera.open();
        try {
            mCamera.setPreviewDisplay(arg0);
            mCamera.setPreviewCallback(this);
        }
        catch (IOException e) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        stopWorkerThread();
        if(mCamera != null){
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    public void stopWorkerThread(){
        if(mWorkerThread != null){
            if (mWorkerThread.isAlive())
                mWorkerThread.quitSafely();
        }
    }

    public void stopPreview(){
        if(mCamera != null){
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
        //getCorrectionMatrix();
    }

    @Override
    public void onNewValue(double[] newValue){
        double ppgRed = newValue[0];
        double ppgGreen = newValue[1];

        if (mCamera != null) {
            Log.i(TAG, "Exposure: " + mCamera.getParameters().getExposureCompensation());
            //Log.i(TAG, "ISO: " + mCamera.getParameters().get("iso"));
        }
        if(ppgSeries != null) {
            ppgSeries.addSample(ppgGreen);
            ((AdvancedLineAndPointRenderer) ppgPlot.
                    getRenderer(AdvancedLineAndPointRenderer.class)).setLatestIndex(ppgSeries.getLatestIndex());
        }
        calculateHeartRate_Spo2 (ppgGreen, ppgRed);
    }

    public void calculateHeartRate_Spo2 (double ppgGreen, double ppgRed){
        ppgListGreen.add(ppgGreen);
        ppgListRed.add(ppgRed);
        int calculatedSignalSize = activity.getResources().getInteger(R.integer.calculated_ppg_window_size);
        if(ppgListGreen.size() == calculatedSignalSize && ppgListRed.size() == calculatedSignalSize){
            double[] ppgArrayGreen =  new double[ppgListGreen.size()];
            double[] ppgArrayRed =  new double[ppgListRed.size()];
            for (int i = 0; i < ppgListGreen.size(); i++) {
                ppgArrayGreen[i] = ppgListGreen.get(i);
            }
            for (int i = 0; i < ppgListRed.size(); i++) {
                ppgArrayRed[i] = ppgListRed.get(i);
            }
            double[][] ppgRedGreen = {ppgArrayRed,ppgArrayGreen};
            if (!isStopped){
                heartRateThread.queueTask(count,  ppgArrayGreen);
                spo2Thread.queueTask(count,  ppgRedGreen);
                Log.d(TAG, "Current time: "  + System.currentTimeMillis());
            }
            ppgListGreen = ppgListGreen.subList(30,ppgListGreen.size()-1);
            ppgListRed = ppgListRed.subList(30,ppgListRed.size()-1);

            //getCorrectionMatrix();
        }


    }

    @Override
    public void onNewHeartRateValue(double newHeartRateValue) {
        hr_textview.setText(activity.getString(R.string.heartrate_textview)+ "\n" + newHeartRateValue);
    }

    @Override
    public void onNewSpo2Value(double newSpo2Value) {

        if(!Double.isNaN(newSpo2Value)){
            int convetedSpo2 = (int)(100 * newSpo2Value);
            spo2_textview.setText(activity.getString(R.string.spo2_textview) + "\n" +convetedSpo2);
        }
    }


    /**
     * Special {@link AdvancedLineAndPointRenderer.Formatter} that draws a line
     * that fades over time.  Designed to be used in conjunction with a circular buffer model.
     */
    public static class MyFadeFormatter extends AdvancedLineAndPointRenderer.Formatter {
        private int trailSize;
        public MyFadeFormatter(int trailSize) {
            this.trailSize = trailSize;
        }
        @Override
        public Paint getLinePaint(int thisIndex, int latestIndex, int seriesSize) {
            // offset from the latest index:
            int offset;
            if(thisIndex > latestIndex) {
                offset = latestIndex + (seriesSize - thisIndex);
            } else {
                offset =  latestIndex - thisIndex;
            }

            float scale = 255f / trailSize;
            int alpha = (int) (255 - (offset * scale));
            getLinePaint().setAlpha(alpha > 0 ? alpha : 0);
            return getLinePaint();
        }
    }

    public synchronized void requestPreviewFrame() {
        if (mCamera != null) {
            mCamera.setOneShotPreviewCallback(new Camera.PreviewCallback() {
                @Override
                public void onPreviewFrame(byte[] data, Camera camera) {
                    try {
                        int[] rgbImage = YUVImageToRGBImage(data, PreviewSizeWidth, PreviewSizeHeight);
                        Bitmap bmpImage = Bitmap.createBitmap(rgbImage, PreviewSizeWidth, PreviewSizeHeight, Bitmap.Config.ARGB_8888);
                        Log.i(TAG,"RGB image size = " + rgbImage.length);
                        File file = new File(Environment.getExternalStorageDirectory().getPath() + "/PPGapp/" +  "capturedChecker.png");
                        FileOutputStream filecon = new FileOutputStream(file);
                        bmpImage.compress(Bitmap.CompressFormat.PNG, 100, filecon);
                        filecon.flush();
                        filecon.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private static void bitmapToFile(Bitmap bitmap, String fileName){
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        String MyDirectory_path = extStorageDirectory ;
        final String photoPath      = MyDirectory_path + "/" + fileName;

        File album = new File(MyDirectory_path);
        if (!album.isDirectory() && !album.mkdirs()) {
            Log.e(TAG, "Failed to create album directory at " + MyDirectory_path);
            return;
        }
        OutputStream outputStream;

        try {
            outputStream = new FileOutputStream(photoPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        Log.i(TAG, "Image saved at" + MyDirectory_path);
    }

    public native int[] YUVImageToRGBImage(byte[] data, int width, int height);
}