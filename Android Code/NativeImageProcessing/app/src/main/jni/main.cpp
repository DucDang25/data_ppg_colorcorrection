#include <jni.h>
#include "my_project_nativeimageprocessing_ImageProcessingThread.hpp"
#include "testfunction.h"
#include <opencv2/opencv.hpp>
#include <set>
#include "persistence1d.hpp"
using namespace std;
using namespace cv;

JNIEXPORT jdoubleArray JNICALL Java_my_project_nativeimageprocessing_ImageProcessingThread_calculatingPPG
        (JNIEnv *env, jobject obj, jbyteArray data, jint frameWidth, jint frameHeight) {

    jboolean  isCopy;
    jbyte * cData = env->GetByteArrayElements(data, &isCopy);

    int width = frameWidth;
    int height = frameHeight;
    int size = width*height;
    int u,v,y1,y2,y3,y4;

    double greenAverage = 0;
    double redAverage = 0;
    double blueAverage = 0;
    for(int i = 0, k = 0; i < size; i+=2, k+=2){
        y1 = cData[i] & 0xff;
        y2 = cData[i+1] & 0xff;
        y3 = cData[width+i] & 0xff;
        y4 = cData[width+i+1] & 0xff;
        v = cData[size+k] & 0xff;
        u = cData[size+k+1] & 0xff;
        v = v - 128;
        u = u - 128;
        greenAverage += calculatingGreen(y1, u, v);
        greenAverage += calculatingGreen(y2, u, v);
        greenAverage += calculatingGreen(y3, u, v);
        greenAverage += calculatingGreen(y4, u, v);
        redAverage += calculatingRed(y1, u, v);
        redAverage += calculatingRed(y2, u, v);
        redAverage += calculatingRed(y3, u, v);
        redAverage += calculatingRed(y4, u, v);
        blueAverage += calculatingBlue(y1, u, v);
        blueAverage += calculatingBlue(y2, u, v);
        blueAverage += calculatingBlue(y3, u, v);
        blueAverage += calculatingBlue(y4, u, v);
        if (i!=0 && (i+2)%width == 0){
            i += width;
        }
    }

    redAverage = redAverage/(size);
    greenAverage = greenAverage/(size);
    blueAverage = blueAverage/(size);
    jdoubleArray avgValue;
    avgValue = (*env).NewDoubleArray(3);
    jdouble tempArray[3];
    tempArray[0] = redAverage;
    //tempArray[0] = 0;
    tempArray[1] = greenAverage;
    tempArray[2] = blueAverage;
    (*env).SetDoubleArrayRegion(avgValue,0,3,tempArray);
    (*env).ReleaseByteArrayElements(data,cData,0);
    return avgValue;
}

JNIEXPORT jdoubleArray JNICALL Java_my_project_nativeimageprocessing_ImageProcessingThread_calculatingPPGColorCorrected
        (JNIEnv *env, jobject obj, jbyteArray data, jint frameWidth, jint frameHeight, jlong corrMatAddr) {
    Mat& corrMat = *(Mat*)corrMatAddr;
    jboolean  isCopy;
    jbyte   * cData = env->GetByteArrayElements(data, &isCopy);

    int width = frameWidth;
    int height = frameHeight;
    int size = width*height;
    int u,v,y1,y2,y3,y4;

    int rgbFrame[size];

    YUVToRGBImage(cData, frameWidth,  frameHeight, rgbFrame);
    Mat rgbMat = Mat(frameHeight, frameWidth, CV_8UC4, rgbFrame);

    vector<Mat> planes;
    split(rgbMat,planes);

    Mat redTransposed = planes[1].t();
    Mat redColumn = redTransposed.reshape(1,1).t();
    Mat greenTransposed = planes[2].t();
    Mat greenColumn = greenTransposed.reshape(1,1).t();
    Mat blueTransposed = planes[3].t();
    Mat blueColumn = blueTransposed.reshape(1,1).t();
    Mat onesColumns = Mat::ones(size, 1, redColumn.type());
    Mat matReshapedProg = Mat(size,4,CV_64FC1);
    onesColumns.col(0).copyTo(matReshapedProg.col(0));
    redColumn.col(0).copyTo(matReshapedProg.col(1));
    greenColumn.col(0).copyTo(matReshapedProg.col(2));
    blueColumn.col(0).copyTo(matReshapedProg.col(3));

    Mat matReshapedCorrected = matReshapedProg * corrMat;

    //Mat tempColAvg = reduce(img,col_mean, 1, CV_REDUCE_AVG);

    //cv:Scalar tempValRed = mean( matReshapedCorrected.col(0) );
    //float redMean = tempValRed.val[0];

    cv:Scalar tempValGreen = mean( matReshapedCorrected.col(1) );
    float greenMean = tempValGreen.val[0];

    //Scalar tempValBlue = mean( matReshapedCorrected.col(2) );
    //float blueMean = tempValBlue.val[0];

    redTransposed.release();
    redColumn.release();
    greenTransposed.release();
    greenColumn.release();
    blueTransposed.release();
    blueColumn.release();
    onesColumns.release();
    matReshapedProg.release();
    //planes.release();
    matReshapedCorrected.release();

    jdoubleArray avgValue;
    avgValue = (*env).NewDoubleArray(3);
    jdouble tempArray[3];
    //tempArray[0] = redMean;
    tempArray[0] = 0;

    tempArray[1] = greenMean ;
    //tempArray[2] = blueMean;
    tempArray[2] = 0;

    (*env).SetDoubleArrayRegion(avgValue,0,3,tempArray);
    return avgValue;
}

JNIEXPORT jintArray JNICALL Java_my_project_nativeimageprocessing_CameraPreview_YUVImageToRGBImage
        (JNIEnv *env, jobject obj, jbyteArray data, jint frameWidth, jint frameHeight) {
    int size = frameWidth*frameHeight;
    jboolean  isCopy;
    jbyte   * cData = env->GetByteArrayElements(data, &isCopy);
    //jintArray rgbData = YUVToRGBImage(env, cData, frameWidth,  frameHeight);
    int result[size];
    YUVToRGBImage(cData, frameWidth,  frameHeight, result);

    jintArray rgbData;
    rgbData = (*env).NewIntArray(size);
    (*env).SetIntArrayRegion(rgbData,0,size,result);
    (*env).ReleaseByteArrayElements(data,cData,0);
    return rgbData;
}

void YUVToRGBImage(jbyte * cData, jint frameWidth, jint frameHeight, int result[]){
    int width = frameWidth;
    int height = frameHeight;
    int size = width*height;
    int u,v,y1,y2,y3,y4;

    //int result[size];
    double greenAverage = 0;
    double redAverage   = 0;
    double blueAverage  = 0;
    for(int i = 0, k = 0; i < size; i+=2, k+=2){
        y1 = cData[i] & 0xff;
        y2 = cData[i+1] & 0xff;
        y3 = cData[width+i] & 0xff;
        y4 = cData[width+i+1] & 0xff;
        v = cData[size+k] & 0xff;
        u = cData[size+k+1] & 0xff;
        v = v - 128;
        u = u - 128;
        result[i]         = convertYUVtoRGB(y1, u, v);
        result[i+1]       = convertYUVtoRGB(y2, u, v);
        result[width+i]   = convertYUVtoRGB(y3, u, v);
        result[width+i+1] = convertYUVtoRGB(y4, u, v);
        if (i!=0 && (i+2)%width == 0){
            i += width;
        }
    }
    //return result;
}

int calculatingGreen(int y, int u, int v){
    int green = y - (int)(0.344f*v + 0.714f*u);
    green = green >255? 255 : green < 0 ? 0 : green;
    return green;
}

int calculatingRed(int y, int u, int v){
    int red = y + (int)(1.402f*v);
    red = red >255? 255 : red < 0 ? 0 : red;
    return red;
}

int calculatingBlue(int y, int u, int v){
    int blue = y + (int)(1.772f*u);
    blue = blue >255? 255 : blue < 0 ? 0 : blue;
    return blue;
}

int convertYUVtoRGB(int y, int u, int v){
    int r,g,b;
    r = calculatingRed(y, u, v);
    g = calculatingGreen(y, u, v);
    b = calculatingBlue(y, u, v);
    return 0xff000000 | (r<<16) | (g<<8) | b;
}