
#include "spo2calculation.h"
#include "testfunction.h"
#include <jni.h>
#include "persistence1d.hpp"
#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

JNIEXPORT jdouble JNICALL Java_my_project_nativeimageprocessing_spo2EstimationThread_nativeSpo2Calculation(JNIEnv *env,  jobject obj, jdoubleArray dataRed, jdoubleArray dataGreen){
    //Convert ppgArray to ppgVector
    jboolean  isCopyR;
    jboolean  isCopyG;

    jsize sizeR = env->GetArrayLength(dataRed);
    jsize sizeG = env->GetArrayLength(dataGreen);

    std::vector<double> ppgVectorGreen(0);
    std::vector<double> ppgVectorRed(0);

    jdouble * tempR = env->GetDoubleArrayElements(dataRed,&isCopyR);
    jdouble * tempG = env->GetDoubleArrayElements(dataGreen,&isCopyG);

    //std::vector<double> ppgVector(temp, temp + sizeof(temp)/sizeof (temp[0]));
    for (int i = 0; i < sizeR; i++){
        ppgVectorRed.push_back(tempR[i]);
    }
    for (int i = 0; i < sizeG; i++){
        ppgVectorGreen.push_back(tempG[i]);
    }

    //env->GetDoubleArrayRegion(ppgArray,0,size,&ppgVector);
    // Calculate Heart Rate and return
    if (ppgVectorGreen.size()==100 && ppgVectorRed.size()==100){
        return  spo2(ppgVectorGreen, ppgVectorRed);
            //return ppgVector.size();
    }

    return -1;
}
