

#include <jni.h>
#include <vector>
#include <opencv2/opencv.hpp>

#ifndef NATIVEIMAGEPROCESSING_TESTFUNCTION_H
#define NATIVEIMAGEPROCESSING_TESTFUNCTION_H

float testfunction(std::vector<double>& signalData);
float spo2(std::vector<double>& signalPartGreen, std::vector<double>& signalPartRed);

#endif //NATIVEIMAGEPROCESSING_TESTFUNCTION_H
