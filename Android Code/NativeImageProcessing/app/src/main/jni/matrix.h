//
// Created by ducdang on 4/12/17.
//
#include <jni.h>
#include <opencv2/opencv.hpp>

#ifndef NATIVEIMAGEPROCESSING_MATRIX_H
#define NATIVEIMAGEPROCESSING_MATRIX_H

#ifdef __cplusplus
extern "C" {
#endif


using namespace std;
using namespace cv;

JNIEXPORT void JNICALL Java_my_project_nativeimageprocessing_CameraPreview_colorCorrection_CorrectionMatrixCalculatingActivity_getCorrectionMatrix (JNIEnv * env, jobject obj,
                       jlong matAddrRefChecker, jlong matAddrGenChecker, jlong matAddrGeneratedChecker, jlong matAddrCorrectionMat, jlong matAddrCorrected );

Mat generateCorrectionMatrix(Mat refColorChecker, Mat generatedColorChecker);

Mat generateColorChecker(Mat colorCheckerImage);

#ifdef __cplusplus
}
#endif
#endif //NATIVEIMAGEPROCESSING_MATRIX_H
