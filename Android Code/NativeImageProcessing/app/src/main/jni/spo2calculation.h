
#include <jni.h>

#ifndef NATIVEIMAGEPROCESSING_SPO2CALCULATION_H
#define NATIVEIMAGEPROCESSING_SPO2CALCULATION_H
#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT jdouble JNICALL Java_my_project_nativeimageprocessing_spo2EstimationThread_nativeSpo2Calculation(JNIEnv *env,  jobject obj, jdoubleArray dataRed, jdoubleArray dataGreen);
#ifdef __cplusplus
}
#endif
#endif //NATIVEIMAGEPROCESSING_SPO2CALCULATION_H
