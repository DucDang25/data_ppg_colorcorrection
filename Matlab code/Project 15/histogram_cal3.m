clc; close all; clear all;
rawImg_moto = imread('chart_processed_moto_8.png'); % samsung phone patch 8, motophone patch 8
rawImg_sam = imread('chart_processed_sam_8.png'); % samsung phone patch 8, motophone patch 8

patch_info = load('patch_info');
patch_info = patch_info.patch_info;
patch_info(:,3) = 69;
patch_info(:,4) = 69;
temp = patch_info(:,1);
patch_info(:,1) = patch_info(:,2);
patch_info(:,2) = temp;

nrgb = 24;
rgb = zeros(nrgb,3); % holds the RGB color of each patch
rgb(1,:)  = [115 82 68];
rgb(2,:)  = [194 150 130];
rgb(3,:)  = [98 122 157];
rgb(4,:)  = [87 108 67];
rgb(5,:)  = [133 128 177];
rgb(6,:)  = [103 189 170];
rgb(7,:)  = [214 126 44];
rgb(8,:)  = [80 91 166];
rgb(9,:)  = [193 90 99];
rgb(10,:) = [94 60 108];
rgb(11,:) = [157 188 64];
rgb(12,:) = [224 163 46];
rgb(13,:) = [56 61 150];
rgb(14,:) = [70 148 73 ];
rgb(15,:) = [175 54 60];
rgb(16,:) = [231 199 31];
rgb(17,:) = [187 86 149];
rgb(18,:) = [8 133 161];
rgb(19,:) = [243 243 242];
rgb(20,:) = [200 200 200];
rgb(21,:) = [160 160 160];
rgb(22,:) = [122 122 121];
rgb(23,:) = [85 85 85];
rgb(24,:) = [52 52 52];







for i = 1:24
    patch_moto(:,:,:,i) = imcrop(rawImg_moto, patch_info(i,:));
    patch_sam(:,:,:,i) = imcrop(rawImg_sam, patch_info(i,:));
end

for i = 1:24
    close all;
    fileName = ['hist_noFlash_',num2str(i),'R_.png'];
    figure;
    temp = round(patch_moto(:,:,1,i)); 
    hist(double(temp(:)));
    hold on
    temp = patch_sam(:,:,1,i);
    hist(double(temp(:)));
    xlim([0 255])
    h = findobj(gca,'Type','patch');
    set(h(1),'Facecolor',[0 0 1],'EdgeColor','k');
    set(h(2),'Facecolor',[1 0 0],'EdgeColor','k');
    title('Red channel')
    xv = [rgb(i,1) rgb(i,1)]; yv = [0 70*70];
    plot(xv,yv,'k');
    legend('Motorola G4','Samsung Galaxy S6 Edge','Reference value');
    hold off;
    saveas(gcf,fileName);
    
    close all;
    fileName = ['hist_noFlash_',num2str(i),'G_.png'];
    figure;
    temp = round(patch_moto(:,:,2,i)); 
    hist(double(temp(:)));
    hold on
    temp = patch_sam(:,:,2,i);
    hist(double(temp(:)));
    xlim([0 255])
    h = findobj(gca,'Type','patch');
    set(h(1),'Facecolor',[0 0 1],'EdgeColor','k');
    set(h(2),'Facecolor',[1 0 0],'EdgeColor','k');
    title('Green channel')
    xv = [rgb(i,2) rgb(i,2)]; yv = [0 70*70];
    plot(xv,yv,'k');
    legend('Motorola G4','Samsung Galaxy S6 Edge','Reference value');
    hold off;
    saveas(gcf,fileName);    
    
    close all;
    fileName = ['hist_noFlash_',num2str(i),'B_.png'];
    figure;
    temp = round(patch_moto(:,:,3,i)); 
    hist(double(temp(:)));
    hold on
    temp = patch_sam(:,:,3,i);
    hist(double(temp(:)));
    xlim([0 255])
    h = findobj(gca,'Type','patch');
    set(h(1),'Facecolor',[0 0 1],'EdgeColor','k');
    set(h(2),'Facecolor',[1 0 0],'EdgeColor','k');
    title('Blue channel')
    xv = [rgb(i,3) rgb(i,3)]; yv = [0 70*70];
    plot(xv,yv,'k');
    legend('Motorola G4','Samsung Galaxy S6 Edge','Reference value');
    hold off;
    saveas(gcf,fileName);    
end