clc; close all; clear all;
rawImg_moto = imread('chart_processed_moto_8.png'); % samsung phone patch 8, motophone patch 8
rawImg_sam = imread('chart_processed_sam_8.png'); % samsung phone patch 8, motophone patch 8

patch_info = load('patch_info');
patch_info = patch_info.patch_info;
patch_info(:,3) = 69;
patch_info(:,4) = 69;
temp = patch_info(:,1);
patch_info(:,1) = patch_info(:,2);
patch_info(:,2) = temp;
sz = [70*70 2 3];

bluePatch_ref_R = 56;
bluePatch_ref_G = 61;
bluePatch_ref_B = 150;

greenPatch_ref_R = 70;
greenPatch_ref_G = 148;
greenPatch_ref_B = 73;

redPatch_ref_R = 175;
redPatch_ref_G = 54;
redPatch_ref_B = 60;

for i = 1:24
    patch_moto(:,:,:,i) = imcrop(rawImg_moto, patch_info(i,:));
    patch_sam(:,:,:,i) = imcrop(rawImg_sam, patch_info(i,:));
end

bluePatch_redChannel = zeros(sz);
bluePatch_greenChannel = zeros(sz);
bluePatch_blueChannel = zeros(sz);

greenPatch_redChannel = zeros(sz);
greenPatch_greenChannel = zeros(sz);
greenPatch_blueChannel = zeros(sz);

redPatch_redChannel = zeros(sz);
redPatch_greenChannel = zeros(sz);
redPatch_blueChannel = zeros(sz);

%% Blue Patch
temp = round(patch_sam(:,:,1,13)); 
bluePatch_redChannel(:,1,1) = temp(:);
temp = round(patch_moto(:,:,1,13)); 
bluePatch_redChannel(:,2,1) = temp(:);

temp = round(patch_sam(:,:,2,13)); 
bluePatch_greenChannel(:,1,2) = temp(:);
temp = round(patch_moto(:,:,2,13)); 
bluePatch_greenChannel(:,2,2) = temp(:);

temp = round(patch_sam(:,:,3,13)); 
bluePatch_blueChannel(:,1,3) = temp(:);
temp = round(patch_moto(:,:,3,13)); 
bluePatch_blueChannel(:,2,3) = temp(:);

%% Green Patch
temp = round(patch_sam(:,:,1,14)); 
greenPatch_redChannel(:,1,1) = temp(:);
temp = round(patch_moto(:,:,1,14)); 
greenPatch_redChannel(:,2,1) = temp(:);

temp = round(patch_sam(:,:,2,14)); 
greenPatch_greenChannel(:,1,2) = temp(:);
temp = round(patch_moto(:,:,2,14)); 
greenPatch_greenChannel(:,2,2) = temp(:);

temp = round(patch_sam(:,:,3,14)); 
greenPatch_blueChannel(:,1,3) = temp(:);
temp = round(patch_moto(:,:,3,14)); 
greenPatch_blueChannel(:,2,3) = temp(:);

%% Red Patch
temp = round(patch_sam(:,:,1,15)); 
redPatch_redChannel(:,1,1) = temp(:);
temp = round(patch_moto(:,:,1,15)); 
redPatch_redChannel(:,2,1) = temp(:);

temp = round(patch_sam(:,:,2,15)); 
redPatch_greenChannel(:,1,2) = temp(:);
temp = round(patch_moto(:,:,2,15)); 
redPatch_greenChannel(:,2,2) = temp(:);

temp = round(patch_sam(:,:,3,15)); 
redPatch_blueChannel(:,1,3) = temp(:);
temp = round(patch_moto(:,:,3,15)); 
redPatch_blueChannel(:,2,3) = temp(:);


%% 
close all;
fileName = ['hist_noFlash_bluePatch_',num2str(i),'R_.png'];
figure, hist(bluePatch_redChannel(:,:,1));
hist(bluePatch_redChannel(:,1,1));
hold on
hist(bluePatch_redChannel(:,2,1));
xlim([0 255])
h = findobj(gca,'Type','patch');
set(h(1),'Facecolor',[1 0 0],'EdgeColor','k');
set(h(2),'Facecolor',[0 0 1],'EdgeColor','k');
title('red channel')
legend('Samsung Galaxy S6 Edge','Motorola G4');
saveas(gcf,fileName);

xv = [0 0]; yv = [-10 10];
plot(xh,yh,xv,yv)

hist(data1);
hold on
hist(data2);
h = findobj(gca,'Type','patch');
set(h(1),'Facecolor',[1 0 0],'EdgeColor','k');
set(h(2),'Facecolor',[0 0 1],'EdgeColor','k');


for i = 1:24
    fileName = ['hist_sam_noFlash_',num2str(i),'R_.png'];
    close all;
    figure, histogram(patch(:,:,1,i));
    title('red channel')
    xlim([0 255])
    saveas(gcf,fileName);
    
    fileName = ['hist_sam_noFlash_',num2str(i),'G_.png'];
    close all;
    figure, histogram(patch(:,:,2,i));
    title('Green channel')
    xlim([0 255])
    saveas(gcf,fileName);    
    
    fileName = ['hist_sam_noFlash_',num2str(i),'B_.png'];
    close all;
    figure, histogram(patch(:,:,3,i));
    title('Blue channel')
    xlim([0 255])
    saveas(gcf,fileName);    
end
