Patch      1
red_mean     = 115.09
red_variance =   6.30
red_deviation =   2.51
green_mean   =  52.39
green_variance =   3.66
green_deviation =   1.91
blue_mean      =  30.67
blue_variance  =   4.03
blue_deviation =   2.01





Patch      2
red_mean     = 231.28
red_variance =   4.36
red_deviation =   2.09
green_mean   = 137.21
green_variance =   5.90
green_deviation =   2.43
blue_mean      = 103.24
blue_variance  =   9.90
blue_deviation =   3.15





Patch      3
red_mean     = 105.00
red_variance =  10.28
red_deviation =   3.21
green_mean   =  84.00
green_variance =   3.78
green_deviation =   1.94
blue_mean      =  99.05
blue_variance  =   9.90
blue_deviation =   3.15





Patch      4
red_mean     =  87.17
red_variance =   7.83
red_deviation =   2.80
green_mean   =  72.86
green_variance =   4.26
green_deviation =   2.06
blue_mean      =  36.61
blue_variance  =   5.08
blue_deviation =   2.25





Patch      5
red_mean     = 164.98
red_variance =   9.46
red_deviation =   3.08
green_mean   =  97.08
green_variance =   4.64
green_deviation =   2.15
blue_mean      = 118.46
blue_variance  =  10.07
blue_deviation =   3.17





Patch      6
red_mean     = 112.27
red_variance =   7.63
red_deviation =   2.76
green_mean   = 145.15
green_variance =   5.12
green_deviation =   2.26
blue_mean      = 124.42
blue_variance  =   8.43
blue_deviation =   2.90





Patch      7
red_mean     = 247.38
red_variance =   4.07
red_deviation =   2.02
green_mean   = 128.09
green_variance =   3.38
green_deviation =   1.84
blue_mean      =  53.03
blue_variance  =   6.78
blue_deviation =   2.60





Patch      8
red_mean     =  78.49
red_variance =   9.31
red_deviation =   3.05
green_mean   =  49.41
green_variance =   3.27
green_deviation =   1.81
blue_mean      = 104.00
blue_variance  =   5.82
blue_deviation =   2.41





Patch      9
red_mean     = 254.98
red_variance =   0.04
red_deviation =   0.19
green_mean   =  86.70
green_variance =   3.02
green_deviation =   1.74
blue_mean      =  69.37
blue_variance  =   2.73
blue_deviation =   1.65





Patch     10
red_mean     = 134.35
red_variance =  13.05
red_deviation =   3.61
green_mean   =  40.86
green_variance =   3.86
green_deviation =   1.96
blue_mean      =  58.50
blue_variance  =   3.41
blue_deviation =   1.85





Patch     11
red_mean     = 190.74
red_variance =   6.77
red_deviation =   2.60
green_mean   = 184.67
green_variance =   5.75
green_deviation =   2.40
blue_mean      =  74.29
blue_variance  =   2.63
blue_deviation =   1.62





Patch     12
red_mean     = 246.62
red_variance =   4.65
red_deviation =   2.16
green_mean   = 164.64
green_variance =   6.15
green_deviation =   2.48
blue_mean      =  62.72
blue_variance  =   4.66
blue_deviation =   2.16





Patch     13
red_mean     =  35.95
red_variance =   9.50
red_deviation =   3.08
green_mean   =  16.45
green_variance =   6.12
green_deviation =   2.47
blue_mean      =  62.84
blue_variance  =   7.13
blue_deviation =   2.67





Patch     14
red_mean     =  74.47
red_variance =   6.62
red_deviation =   2.57
green_mean   = 123.01
green_variance =   6.37
green_deviation =   2.52
blue_mean      =  57.05
blue_variance  =   4.94
blue_deviation =   2.22





Patch     15
red_mean     = 254.89
red_variance =   0.15
red_deviation =   0.39
green_mean   =  69.59
green_variance =   1.59
green_deviation =   1.26
blue_mean      =  50.41
blue_variance  =   2.51
blue_deviation =   1.58





Patch     16
red_mean     = 255.00
red_variance =   0.00
red_deviation =   0.00
green_mean   = 223.03
green_variance =   0.99
green_deviation =   1.00
blue_mean      =  75.42
blue_variance  =   3.18
blue_deviation =   1.78





Patch     17
red_mean     = 254.42
red_variance =   1.06
red_deviation =   1.03
green_mean   =  87.49
green_variance =   2.45
green_deviation =   1.57
blue_mean      = 115.14
blue_variance  =   2.42
blue_deviation =   1.56





Patch     18
red_mean     =  42.34
red_variance =  16.54
red_deviation =   4.07
green_mean   =  86.48
green_variance =   5.98
green_deviation =   2.44
blue_mean      = 111.20
blue_variance  =   6.34
blue_deviation =   2.52





Patch     19
red_mean     = 254.70
red_variance =   0.34
red_deviation =   0.58
green_mean   = 219.03
green_variance =   2.18
green_deviation =   1.48
blue_mean      = 198.63
blue_variance  =   1.29
blue_deviation =   1.13





Patch     20
red_mean     = 235.50
red_variance =   1.95
red_deviation =   1.40
green_mean   = 194.59
green_variance =   1.73
green_deviation =   1.31
blue_mean      = 173.68
blue_variance  =   1.98
blue_deviation =   1.41





Patch     21
red_mean     = 213.50
red_variance =   2.49
red_deviation =   1.58
green_mean   = 164.04
green_variance =   1.62
green_deviation =   1.27
blue_mean      = 140.75
blue_variance  =   1.61
blue_deviation =   1.27





Patch     22
red_mean     = 161.40
red_variance =   4.03
red_deviation =   2.01
green_mean   = 111.96
green_variance =   2.18
green_deviation =   1.48
blue_mean      =  89.66
blue_variance  =   1.68
blue_deviation =   1.30





Patch     23
red_mean     =  89.55
red_variance =   4.52
red_deviation =   2.13
green_mean   =  60.28
green_variance =   2.22
green_deviation =   1.49
blue_mean      =  46.99
blue_variance  =   2.84
blue_deviation =   1.69





Patch     24
red_mean     =  36.13
red_variance =   3.87
red_deviation =   1.97
green_mean   =  21.85
green_variance =   2.92
green_deviation =   1.71
blue_mean      =  19.25
blue_variance  =   3.07
blue_deviation =   1.75





