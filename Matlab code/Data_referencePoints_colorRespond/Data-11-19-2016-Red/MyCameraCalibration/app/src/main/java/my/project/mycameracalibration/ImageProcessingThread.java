package my.project.mycameracalibration;

/**
 * Created by ducdang on 10/24/16.
 */
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ImageProcessingThread extends HandlerThread {

    private Handler mWorkerHandler;
    private Handler mRespondHandler;
    private static final String TAG = ImageProcessingThread.class.getSimpleName();
    private List<Double> average;
    private List<Double> rawDataRGBList;
    private List<Double> rawDataYUVList;
    private File dataFileRGB;
    private File dataFileYUV;
    private BufferedWriter writerRGB;
    private BufferedWriter writerYUV;
    private OutputStream outputStreamRGB;
    private OutputStream outputStreamYUV;
    private int count;
    private int count2 = 0;
    private Callback mCallBack;
    private ArrayList<referencePoint> pointsList;

    public interface Callback {
        public void onNewValue (double newValue);
    }

    public ImageProcessingThread(Handler respondHandler, Callback callBack) {
        super(TAG);
        average = new CopyOnWriteArrayList<>();
        initializeRawWriter();
        pointsList = new ArrayList<>();
        mRespondHandler = respondHandler;
        mCallBack = callBack;
    }

    private void initializeRawWriter(){
        rawDataRGBList = new CopyOnWriteArrayList<>();
        rawDataYUVList = new CopyOnWriteArrayList<>();

        try {
            String dataFileNameRGB = "dataRGB.txt";
            String dataFileNameYUV = "dataYUV.txt";
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            String MyDirectory_path = extStorageDirectory + "/" + "MyCamera";
            final String dataPathRGB = MyDirectory_path + "/" + dataFileNameRGB;
            final String dataPathYUV = MyDirectory_path + "/" + dataFileNameYUV;

            File dataFolder = new File(MyDirectory_path);
            if (!dataFolder.isDirectory() && !dataFolder.mkdirs()) {
                Log.e(TAG, "Failed to create app data directory at " + MyDirectory_path);
            }

            dataFileRGB = new File(dataPathRGB);
            dataFileYUV = new File(dataPathYUV);

            outputStreamRGB       = new FileOutputStream(dataPathRGB);
            outputStreamYUV       = new FileOutputStream(dataPathYUV);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void queueTask(int count, int PreviewSizeWidth, int PreviewSizeHeight, byte[] data) {
        //count = 0;
        Log.i(TAG, "New frame queued");
        mWorkerHandler.obtainMessage(count, PreviewSizeWidth, PreviewSizeHeight, data).sendToTarget();
    }

    // NEW
    private static byte[] integersToBytes2(int[] src) {
        int srcLength = src.length;
        byte[] dst = new byte[srcLength << 2];

        for (int i = 0; i < srcLength; i++) {
            int x = src[i];
            int j = i << 2;
            dst[j++] = (byte) ((x >>> 0) & 0xff);
            dst[j++] = (byte) ((x >>> 8) & 0xff);
            dst[j++] = (byte) ((x >>> 16) & 0xff);
            dst[j++] = (byte) ((x >>> 24) & 0xff);
        }
        return dst;
    }

    public void prepareHandler() {
        mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                count = msg.what;
                int PreviewSizeWidth = msg.arg1;
                int PreviewSizeHeight = msg.arg2;
                byte[] data = (byte[]) msg.obj;
                count2++;
                //final double tempResult = 0;
                final double tempResult = (-1)*colorConversion(data, PreviewSizeWidth, PreviewSizeHeight);
                // saveRawDataAndColorConversion(data, PreviewSizeWidth, PreviewSizeHeight);
                average.add(tempResult);

                collectRefPoints(data, PreviewSizeWidth, PreviewSizeHeight);
                //Bitmap bm = Bitmap.createBitmap(YUVtoRGB(data, PreviewSizeWidth, PreviewSizeHeight), PreviewSizeWidth, PreviewSizeHeight, Bitmap.Config.ARGB_8888);
                //bitmapToFile(bm);
                //Log.d(TAG, "Frame Number " + count2 );
                Log.d(TAG, "Average value: " + tempResult );


                mRespondHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mCallBack.onNewValue(tempResult);
                    }
                });


                return true;
            }
        });
    }
    private void toRefPointDataFile(){
        int frameNum = 0;
        try {
            String dataFileName = "refPointData_ex_g4_12.txt";
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            String MyDirectory_path = extStorageDirectory + "/" + "MyCamera";
            final String dataPath = MyDirectory_path + "/" + dataFileName;

            File dataFolder = new File(MyDirectory_path);
            if (!dataFolder.isDirectory() && !dataFolder.mkdirs()) {
                Log.e(TAG, "Failed to create app data directory at " + MyDirectory_path);
            }

            File dataFile = new File(dataPath);
            FileWriter writer = new FileWriter(dataFile);
            Iterator<referencePoint> iterator = pointsList.iterator();
            while (iterator.hasNext()) {
                referencePoint refpoint = iterator.next();
                if(refpoint.position == 1){
                    frameNum++;
                    writer.append("Frame#: " + frameNum); writer.write("\n");
                }


                writer.append("Point: " + refpoint.position.toString()); writer.write("\n");
                writer.append("Y = " + refpoint.y.toString()); writer.write("\n");
                writer.append("U = " + refpoint.u.toString()); writer.write("\n");
                writer.append("V = " + refpoint.v.toString()); writer.write("\n");
                writer.append("R = " + refpoint.r.toString()); writer.write("\n");
                writer.append("G = " + refpoint.g.toString()); writer.write("\n");
                writer.append("B = " + refpoint.b.toString()); writer.write("\n");
                writer.write("\n");
            }
            Log.d(TAG, "Data stored at: " + MyDirectory_path);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void toDataFile(){
        try {
            String dataFileName = "data17ex-3.txt";
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            String MyDirectory_path = extStorageDirectory + "/" + "MyCamera";
            final String dataPath = MyDirectory_path + "/" + dataFileName;

            File dataFolder = new File(MyDirectory_path);
            if (!dataFolder.isDirectory() && !dataFolder.mkdirs()) {
                Log.e(TAG, "Failed to create app data directory at " + MyDirectory_path);
            }
            File dataFile = new File(dataPath);
            FileWriter writer = new FileWriter(dataFile);
            Iterator<Double> iterator = average.iterator();
            while (iterator.hasNext()) {
                Double avg = iterator.next();
                writer.append(avg.toString());
                writer.write("\n");
            }
            Log.d(TAG, "Data stored at: " + MyDirectory_path);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean quitSafely (){
        while (mWorkerHandler.hasMessages(count) ){
            Log.d(TAG, "Frame Number: " + count2 );
        }
        toDataFile();
        toRefPointDataFile();
        return super.quitSafely();
    }


    private int[] YUVtoRGB(byte[] data, int width, int height){
        int size = width*height;
        int u,v,y1,y2,y3,y4;
        int[] result = new int[size];

        for(int i = 0, k = 0; i < size; i+=2, k+=2){
            y1 = data[i] & 0xff;
            y2 = data[i+1] & 0xff;
            y3 = data[width+i] & 0xff;
            y4 = data[width+i+1] & 0xff;

            v = data[size+k] & 0xff;
            u = data[size+k+1] & 0xff;
            v = v - 128;
            u = u - 128;

            result[i]         = convertYUVtoRGB(y1, u, v);
            result[i+1]       = convertYUVtoRGB(y2, u, v);
            result[width+i]   = convertYUVtoRGB(y3, u, v);
            result[width+i+1] = convertYUVtoRGB(y4, u, v);

            if (i!=0 && (i+2)%width == 0){
                i += width;
            }
        }
        return result;
    }

    private int calculatingGreen(int y, int u, int v){
        // int green = y - (int)(0.338f*u + 0.698f*v);
        int green = y - (int)(0.344f*v + 0.714f*u);
        green = green >255? 255 : green < 0 ? 0 : green;
        return green;
    }

    private int convertYUVtoRGB(int y, int u, int v){
        int r,g,b;
        r = y + (int)(1.402f*u);
        g = y - (int)(0.344f*v + 0.714f*u);
        b = y + (int)(1.772f*v);

        r = r >255? 255 : r<0 ? 0 : r;
        g = g >255? 255 : g<0 ? 0 : g;
        b = b >255? 255 : b<0 ? 0 : b;

        return 0xff000000 | (r<<16) | (g<<8) | b;
    }

    private double colorConversion(byte[] data, int width, int height){
        int size = width*height;
        int u,v,y1,y2,y3,y4;

        double greenAverage = 0;
        for(int i = 0, k = 0; i < size; i+=2, k+=2){
            y1 = data[i] & 0xff;
            y2 = data[i+1] & 0xff;
            y3 = data[width+i] & 0xff;
            y4 = data[width+i+1] & 0xff;

            v = data[size+k] & 0xff;
            u = data[size+k+1] & 0xff;
            v = v - 128;
            u = u - 128;

            greenAverage += calculatingGreen(y1, u, v);
            greenAverage += calculatingGreen(y2, u, v);
            greenAverage += calculatingGreen(y3, u, v);
            greenAverage += calculatingGreen(y4, u, v);

            if (i!=0 && (i+2)%width == 0){
                i += width;
            }
        }
        greenAverage = greenAverage/(size);
        return greenAverage;
    }


    private void collectRefPoints (byte[] data, int width, int height){
        int size = width*height;
        int u,v,y1,y2,y3,y4;
        for(int i = 0, k = 0; i < size; i+=2, k+=2){
            y1 = data[i] & 0xff;
            y2 = data[i+1] & 0xff;
            y3 = data[width+i] & 0xff;
            y4 = data[width+i+1] & 0xff;

            v = data[size+k] & 0xff;
            u = data[size+k+1] & 0xff;
            v = v - 128;
            u = u - 128;

            referencePoint tempRefpoint = null;
            int tempRGB = 0;
            if (i == 0 ){
                addRefPoint( y1, u, v, 1);
            } else if(i == 238*width){
                addRefPoint( y1, u, v, 4);
            } else if(i == 478*width) {
                addRefPoint( y1, u, v, 7);
            } else if(i == 318){
                addRefPoint( y4, u, v, 2);
            } else if(i == 238*width+318) {
                addRefPoint( y4, u, v, 5);
            } else if(i == 478*width+318) {
                addRefPoint( y4, u, v, 8);
            } else if(i == 638){
                addRefPoint( y2, u, v, 3);
            } else if(i == 238*width+638){
                addRefPoint( y2, u, v, 6);
            } else if(i == 478*width+638){
                addRefPoint( y2, u, v, 9);
            }

            if (i!=0 && (i+2)%width == 0){
                i += width;
            }
        }
    }

    private void addRefPoint(int y, int u, int v, int position){
        int tempRGB = convertYUVtoRGB(y, u, v);
        referencePoint tempRefpoint = new referencePoint(y, u, v, (tempRGB & 0x000000ff),
                (tempRGB & 0x0000ff00) >> 8, (tempRGB & 0x00ff0000) >> 16, position);
        pointsList.add(tempRefpoint);
    }
}