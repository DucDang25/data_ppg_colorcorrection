clc; close all; clear all;
n=20;
nr = 320;
nc = 476;
orig = imread('chart_classic.png');
sz = [size(orig) 20];
prog = zeros(sz);
prog(:,:,:,1)  = imread('chart_processed_sam_1.png');
prog(:,:,:,2)  = imread('chart_processed_sam_2.png');
prog(:,:,:,3)  = imread('chart_processed_sam_3.png');
prog(:,:,:,4)  = imread('chart_processed_sam_4.png');
prog(:,:,:,5)  = imread('chart_processed_sam_5.png');
prog(:,:,:,6)  = imread('chart_processed_sam_6.png');
prog(:,:,:,7)  = imread('chart_processed_sam_7.png');
prog(:,:,:,8)  = imread('chart_processed_sam_8.png');
prog(:,:,:,9)  = imread('chart_processed_sam_9.png');
prog(:,:,:,10) = imread('chart_processed_sam_10.png');
prog(:,:,:,11) = imread('chart_processed_moto_1.png');
prog(:,:,:,12) = imread('chart_processed_moto_2.png');
prog(:,:,:,13) = imread('chart_processed_moto_3.png');
prog(:,:,:,14) = imread('chart_processed_moto_4.png');
prog(:,:,:,15) = imread('chart_processed_moto_5.png');
prog(:,:,:,16) = imread('chart_processed_moto_6.png');
prog(:,:,:,17) = imread('chart_processed_moto_7.png');
prog(:,:,:,18) = imread('chart_processed_moto_8.png');
prog(:,:,:,19) = imread('chart_processed_moto_9.png');
prog(:,:,:,20) = imread('chart_processed_moto_10.png');

cal_img = zeros(sz);
cal_img2 = zeros(sz);
cal_img3 = zeros(sz);
proc_mean = zeros([24 4 20]);
cal_mean = zeros([24 3 20]);
a = zeros([4 3 n]);
patch_info = load('patch_info.mat');
patch_info = patch_info.patch_info;
%[cal_img1, a1, fit, proc_mean1, cal_mean(:,:,2), orig_mean] = color_xform(orig, 1, 1, 476, 320, patch_info.patch_info, prog(:,:,:,2), 1, 1, 476, 320, 0, 0);

for i = 1:n;
    [cal_img(:,:,:,i), a(:,:,i), fit, proc_mean(:,:,i), cal_mean(:,:,i), orig_mean] = color_xform(orig, 1, 1, 476, 320, patch_info, prog(:,:,:,i), 1, 1, 476, 320, 0, 0);
    close all;
end

a_samsung = a(:,:,1:10);
a_moto = a(:,:,11:20);
a_avg_moto = mean(a_moto,3);
a_avg_samsung = mean(a_samsung,3);

cal_mean2 = zeros([24 3 20]);
for i = 1:10;
    cal_img_raw = double([ones(nr*nc,1) reshape(permute(prog(:,:,:,i),[3,1,2]), 3, nr*nc)'])*a_avg_samsung;
    cal_img2(:,:,:,i) = reshape(cal_img_raw, nr, nc,3);
    cal_img2(:,:,:,i) = refining_checker_generator(320, 476, cal_img2(:,:,:,i), a_avg_samsung(1,:));    
    for j = 1:24;
        % Processed image
        cr_1 = patch_info(j,1); % top coordinate of processed patch
        cc_1 = patch_info(j,2); % left coordinate of processed patch
        cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
        cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
        cal_mean2(j,:,i) = ...
             mean(reshape(cal_img2(cr_1:cr_2, cc_1:cc_2, :,i),patch_info(j,3)*patch_info(j,4),3));
    end
    cal_img(:,:,:,i) = refining_checker_generator(320, 476, cal_img(:,:,:,i), a(1,:,i));
    fileName = ['cal_img_samsung_',num2str(i),'.png'];
    imwrite(cal_img(:,:,:,i)/255,fileName);

end

for i = 11:20;
    cal_img_raw = double([ones(nr*nc,1) reshape(permute(prog(:,:,:,i),[3,1,2]), 3, nr*nc)'])*a_avg_moto;
    cal_img2(:,:,:,i) = reshape(cal_img_raw, nr, nc,3);
    cal_img2(:,:,:,i) = refining_checker_generator(320, 476, cal_img2(:,:,:,i), a_avg_moto(1,:));    
    for j = 1:24;
        % Processed image
        cr_1 = patch_info(j,1); % top coordinate of processed patch
        cc_1 = patch_info(j,2); % left coordinate of processed patch
        cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
        cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
        cal_mean2(j,:,i) = ...
             mean(reshape(cal_img2(cr_1:cr_2, cc_1:cc_2, :,i),patch_info(j,3)*patch_info(j,4),3));
    end
    cal_img(:,:,:,i) = refining_checker_generator(320, 476, cal_img(:,:,:,i), a(1,:,i));
    fileName = ['cal_img_moto_',num2str(i),'.png'];
    imwrite(cal_img(:,:,:,i)/255,fileName);
end

red_respond   = zeros([24 6 20]);
green_respond = zeros([24 6 20]);
blue_respond  = zeros([24 6 20]);

prog_img_samsung_avg = mean(prog(:,:,:,1:10),4);
prog_img_moto_avg = mean(prog(:,:,:,11:20),4);

cal_img_samsung = cal_img(:,:,:,1:10);
cal_img_samsung_avg = mean(cal_img_samsung,4);
imwrite(cal_img_samsung_avg/255,'cal_avg_img_samsung.png');
cal_img_moto = cal_img(:,:,:,11:20);
cal_img_moto_avg = mean(cal_img_moto,4);
imwrite(cal_img_moto_avg/255,'cal_avg_img_moto.png');

for j = 1:24;
   % Processed image
   cr_1 = patch_info(j,1); % top coordinate of processed patch
   cc_1 = patch_info(j,2); % left coordinate of processed patch
   cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
   cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
   cal_img_samsung_avg_mean(j,:) = mean(reshape(cal_img_samsung_avg(cr_1:cr_2, cc_1:cc_2, :),patch_info(j,3)*patch_info(j,4),3));
   cal_img_moto_avg_mean(j,:) = mean(reshape(cal_img_moto_avg(cr_1:cr_2, cc_1:cc_2, :),patch_info(j,3)*patch_info(j,4),3));
end

index = (1:24)';
for i = 1:10
    red_respond(:,:,i)   = [orig_mean(:,1) cal_mean(:,1,i) proc_mean(:,2,i) cal_mean2(:,1,i) cal_img_samsung_avg_mean(:,1) index];
    red_respond(:,:,i)   = sortrows(red_respond(:,:,i),1);
    green_respond(:,:,i) = [orig_mean(:,2) cal_mean(:,2,i) proc_mean(:,3,i) cal_mean2(:,2,i) cal_img_samsung_avg_mean(:,2) index];
    green_respond(:,:,i) = sortrows(green_respond(:,:,i),1);
    blue_respond(:,:,i)  = [orig_mean(:,3) cal_mean(:,3,i) proc_mean(:,4,i) cal_mean2(:,3,i) cal_img_samsung_avg_mean(:,3) index];
    blue_respond(:,:,i)  = sortrows(blue_respond(:,:,i),1);
end

for i = 11:20
    red_respond(:,:,i)   = [orig_mean(:,1) cal_mean(:,1,i) proc_mean(:,2,i) cal_mean2(:,1,i) cal_img_moto_avg_mean(:,1) index];
    red_respond(:,:,i)   = sortrows(red_respond(:,:,i),1);
    green_respond(:,:,i) = [orig_mean(:,2) cal_mean(:,2,i) proc_mean(:,3,i) cal_mean2(:,2,i) cal_img_moto_avg_mean(:,2) index];
    green_respond(:,:,i) = sortrows(green_respond(:,:,i),1);
    blue_respond(:,:,i)  = [orig_mean(:,3) cal_mean(:,3,i) proc_mean(:,4,i) cal_mean2(:,3,i) cal_img_moto_avg_mean(:,3) index];
    blue_respond(:,:,i)  = sortrows(blue_respond(:,:,i),1);
end

%%
rawImg1 = prog(:,:,:,8);  % samsung phone patch 8, 
rawImg2 = prog(:,:,:,18); % motophone patch 8
patch_info2 = patch_info;
patch_info2(:,3) = 69;
patch_info2(:,4) = 69;
temp = patch_info2(:,1);
patch_info2(:,1) = patch_info2(:,2);
patch_info2(:,2) = temp;

patch1 = zeros([70 70 3 24]);
patch2 = zeros([70 70 3 24]);

for i = 1:24
    patch1(:,:,:,i) = imcrop(rawImg1, patch_info2(i,:));
    patch2(:,:,:,i) = imcrop(rawImg2, patch_info2(i,:));
end

r1_order = zeros(70*70,24);
r2_order = zeros(70*70,24);
g1_order = zeros(70*70,24);
g2_order = zeros(70*70,24);
b1_order = zeros(70*70,24);
b2_order = zeros(70*70,24);

for i = 1:24
    red   = patch1(:,:,1,i);
    green = patch1(:,:,2,i);
    blue  = patch1(:,:,3,i);
    red   = red(:);
    green = green(:);
    blue  = blue(:);
    r1(:,i) = red;
    g1(:,i) = green;
    b1(:,i) = blue;
    
    red   = patch2(:,:,1,i);
    green = patch2(:,:,2,i);
    blue  = patch2(:,:,3,i);
    red   = red(:);
    green = green(:);
    blue  = blue(:);
    r2(:,i) = red;
    g2(:,i) = green;
    b2(:,i) = blue;
end

for i = 1:24
    r1_order(:,i) = r1(:,red_respond(i,6,8));
    g1_order(:,i) = g1(:,green_respond(i,6,8));
    b1_order(:,i) = b1(:,blue_respond(i,6,8));
    r2_order(:,i) = r2(:,red_respond(i,6,18));
    g2_order(:,i) = g2(:,green_respond(i,6,18));
    b2_order(:,i) = b2(:,blue_respond(i,6,18));
end



figure
hold on
boxplot(r1_order)
plot(red_respond(:,1,8));
plot(red_respond(:,3,8));
ylim([0 255])
title('Red')
legend('Reference value','Red channel means, uncorrected image')
hold off

figure
hold on
boxplot(g1_order)
plot(green_respond(:,1,8));
plot(green_respond(:,3,8));
ylim([0 255])
title('Green')
legend('Reference value','Green channel means, uncorrected image')
hold off

figure
hold on
boxplot(b1_order)
plot(blue_respond(:,1,8));
plot(blue_respond(:,3,8));
ylim([0 255])
title('Blue')
legend('Reference value','Blue channel means, uncorrected image')
hold off

figure
hold on
boxplot(r2_order)
plot(red_respond(:,1,18));
plot(red_respond(:,3,18));
ylim([0 255])
title('Red')
legend('Reference value','Red channel means, uncorrected image')
hold off

figure
hold on
boxplot(g2_order)
plot(green_respond(:,1,18));
plot(green_respond(:,3,18));
ylim([0 255])
title('Green')
legend('Reference value','Green channel means, uncorrected image')
hold off

figure
hold on
boxplot(b2_order)
plot(blue_respond(:,1,18));
plot(blue_respond(:,3,18));
ylim([0 255])
title('Blue')
legend('Reference value','Blue channel means, uncorrected image')
hold off

%%



