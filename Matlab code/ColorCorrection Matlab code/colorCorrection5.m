clc; close all; clear all;
orig = imread('chart_classic.png');
prog1 = imread('chart_processed_sam_8.png');
prog2 = imread('chart_processed_moto_8.png');


figure; image(orig);
figure; image(prog1);
figure; image(prog2);

patch_info = load('patch_info.mat');
[cal_img1, a1, fit, proc_mean1, cal_mean1, orig_mean1] = color_xform3(orig, 1, 1, 476, 320, patch_info.patch_info, prog1, 1, 1, 476, 320, 0, 0);
figure; image(cal_img1/255);
[cal_img2, a2, fit, proc_mean2, cal_mean2, orig_mean2] = color_xform3(orig, 1, 1, 476, 320, patch_info.patch_info, prog2, 1, 1, 476, 320, 0, 0);
figure; image(cal_img2/255);

close all; 

finalChart1 = refining_checker_generator(320, 476, cal_img1, a1(1,:));
finalChart2 = refining_checker_generator(320, 476, cal_img2, a2(1,:));

figure; image(finalChart1/255);
figure; image(finalChart2/255);

red_respond   = [orig_mean1(:,1) cal_mean1(:,1) proc_mean1(:,2) proc_mean2(:,2) cal_mean2(:,1)];
red_respond   = sortrows(red_respond,1);
green_respond = [orig_mean1(:,2) cal_mean1(:,2) proc_mean1(:,3) proc_mean2(:,3) cal_mean2(:,2)];
green_respond = sortrows(green_respond,1);
blue_respond  = [orig_mean1(:,3) cal_mean1(:,3) proc_mean1(:,4) proc_mean2(:,4) cal_mean2(:,3)];
blue_respond  = sortrows(blue_respond,1);

figure;
hold on;
plot(red_respond(:,1),red_respond(:,1));
plot(red_respond(:,1),red_respond(:,2));
plot(red_respond(:,1),red_respond(:,5));
legend('Reference value vs Reference value','Corrected Samsung phone value vs Reference value', 'Corrected Motorola phone value vs Reference value')
title('Red channel')
hold off;

figure;
hold on;
plot(green_respond(:,1),green_respond(:,1));
plot(green_respond(:,1),green_respond(:,2));
plot(green_respond(:,1),green_respond(:,5));
legend('Reference value vs Reference value','Corrected Samsung phone value vs Reference value', 'Corrected Motorola phone value vs Reference value')
title('Green channel')
hold off;

figure;
hold on;
plot(blue_respond(:,1),blue_respond(:,1));
plot(blue_respond(:,1),blue_respond(:,2));
plot(blue_respond(:,1),blue_respond(:,5));
legend('Reference value vs Reference value','Corrected Samsung phone value vs Reference value', 'Corrected Motorola phone value vs Reference value')
title('Blue channel')
hold off;

figure;
hold on;
plot(red_respond(:,1),red_respond(:,1));
plot(red_respond(:,1),red_respond(:,3));
plot(red_respond(:,1),red_respond(:,4));
legend('Reference value vs Reference value','Corrected Samsung phone value vs Reference value', 'Corrected Motorola phone value vs Reference value')
legend('Reference value vs Reference value','Samsung phone value vs Reference value', 'Motorola phone value vs Reference value')
title('Red channel')
hold off;

figure;
hold on;
plot(green_respond(:,1),green_respond(:,1));
plot(green_respond(:,1),green_respond(:,3));
plot(green_respond(:,1),green_respond(:,4));
legend('Reference value vs Reference value','Corrected Samsung phone value vs Reference value', 'Corrected Motorola phone value vs Reference value')
legend('Reference value vs Reference value','Samsung phone value vs Reference value', 'Motorola phone value vs Reference value')
title('Green channel')
hold off;

figure;
hold on;
plot(blue_respond(:,1),blue_respond(:,1));
plot(blue_respond(:,1),blue_respond(:,3));
plot(blue_respond(:,1),blue_respond(:,4));
legend('Reference value vs Reference value','Corrected Samsung phone value vs Reference value', 'Corrected Motorola phone value vs Reference value')
legend('Reference value vs Reference value','Samsung phone value vs Reference value', 'Motorola phone value vs Reference value')
title('Blue channel')
hold off;
