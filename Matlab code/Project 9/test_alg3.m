clc; close all; clear all;
orig = imread('chart_classic.png');
prog1 = imread('chart_processed_GalaxyEdge.png');
prog2 = imread('chart_processed_MotoG4.png');


figure; image(orig);
figure; image(prog1);
figure; image(prog2);
nr = 320;
nc = 476;

patch_info = load('patch_info.mat');
[cal_img1, a1, fit, proc_mean1, cal_mean1, orig_mean1] = color_xform(orig, 1, 1, 476, 320, patch_info.patch_info, prog1, 1, 1, 476, 320, 0, 0);
figure; image(cal_img1/255);
[cal_img2, a2, fit, proc_mean2, cal_mean2, orig_mean2] = color_xform(orig, 1, 1, 476, 320, patch_info.patch_info, prog2, 1, 1, 476, 320, 0, 0);
figure; image(cal_img2/255);

close all; 

finalChart1 = refining_checker_generator(320, 476, cal_img1, a1(1,:));
finalChart2 = refining_checker_generator(320, 476, cal_img2, a2(1,:));

red_respond = [orig_mean1(:,1) cal_mean1(:,1) cal_mean2(:,1) proc_mean1(:,2) proc_mean2(:,2)];
red_respond = sortrows(red_respond,1);
green_respond = [orig_mean1(:,2) cal_mean1(:,2) cal_mean2(:,2) proc_mean1(:,3) proc_mean2(:,3)];
green_respond = sortrows(green_respond,1);
blue_respond = [orig_mean1(:,3) cal_mean1(:,3) cal_mean2(:,3) proc_mean1(:,4) proc_mean2(:,4)];
blue_respond = sortrows(blue_respond,1);

%%
figure
hold on
plot( red_respond(:,1),red_respond(:,2));
plot( red_respond(:,1),red_respond(:,3));
hold off

figure
hold on
plot( red_respond(:,1),red_respond(:,4));
plot( red_respond(:,1),red_respond(:,5));
hold off


figure
hold on
plot( green_respond(:,1),green_respond(:,2));
plot( green_respond(:,1),green_respond(:,3));
hold off

figure
hold on
plot( green_respond(:,1),green_respond(:,4));
plot( green_respond(:,1),green_respond(:,5));
hold off

figure
hold on
plot( blue_respond(:,1),blue_respond(:,2));
plot( blue_respond(:,1),blue_respond(:,3));
hold off

figure
hold on
plot( blue_respond(:,1),blue_respond(:,4));
plot( blue_respond(:,1),blue_respond(:,5));
hold off

%%

figure
hold on
plot( red_respond(:,1));
plot( red_respond(:,4));
plot( red_respond(:,5));
hold off


figure
hold on
plot( red_respond(:,1));
plot( red_respond(:,2));
plot( red_respond(:,3));
hold off

avg_moto_R = sum(sum(cal_img1(:,:,1))/(70*70*24));
avg_moto_G = sum(sum(cal_img1(:,:,2))/(70*70*24));
avg_moto_B = sum(sum(cal_img1(:,:,3))/(70*70*24));
avg_moto = [avg_moto_R avg_moto_G avg_moto_B];

avg_sam_R = sum(sum(cal_img2(:,:,1))/(70*70*24));
avg_sam_G = sum(sum(cal_img2(:,:,2))/(70*70*24));
avg_sam_B = sum(sum(cal_img2(:,:,3))/(70*70*24));
avg_sam = [avg_sam_R avg_sam_G avg_sam_B];

avg_prog_moto_R = sum(sum(prog1(:,:,1))/(70*70*24));
avg_prog_moto_G = sum(sum(prog1(:,:,2))/(70*70*24));
avg_prog_moto_B = sum(sum(prog1(:,:,3))/(70*70*24));
avg_prog_moto = [avg_prog_moto_R avg_prog_moto_G avg_prog_moto_B];

avg_prog_sam_R = sum(sum(prog2(:,:,1))/(70*70*24));
avg_prog_sam_G = sum(sum(prog2(:,:,2))/(70*70*24));
avg_prog_sam_B = sum(sum(prog2(:,:,3))/(70*70*24));
avg_prog_sam = [avg_prog_sam_R avg_prog_sam_G avg_prog_sam_B];


