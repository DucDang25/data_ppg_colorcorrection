clc; close all; clear all;
n=20;
nr = 320;
nc = 476;
orig = imread('chart_classic.png');
sz = [size(orig) 20];
prog = zeros(sz);
prog(:,:,:,1) = imread('chart_processed_sam_1.png');
prog(:,:,:,2) = imread('chart_processed_sam_2.png');
prog(:,:,:,3) = imread('chart_processed_sam_3.png');
prog(:,:,:,4) = imread('chart_processed_sam_4.png');
prog(:,:,:,5) = imread('chart_processed_sam_5.png');
prog(:,:,:,6) = imread('chart_processed_sam_6.png');
prog(:,:,:,7) = imread('chart_processed_sam_7.png');
prog(:,:,:,8) = imread('chart_processed_sam_8.png');
prog(:,:,:,9) = imread('chart_processed_sam_9.png');
prog(:,:,:,10) = imread('chart_processed_sam_10.png');
prog(:,:,:,11) = imread('chart_processed_MotoG4_1.png');
prog(:,:,:,12) = imread('chart_processed_MotoG4_2.png');
prog(:,:,:,13) = imread('chart_processed_MotoG4_3.png');
prog(:,:,:,14) = imread('chart_processed_MotoG4_4.png');
prog(:,:,:,15) = imread('chart_processed_MotoG4_5.png');
prog(:,:,:,16) = imread('chart_processed_MotoG4_6.png');
prog(:,:,:,17) = imread('chart_processed_MotoG4_7.png');
prog(:,:,:,18) = imread('chart_processed_MotoG4_8.png');
prog(:,:,:,19) = imread('chart_processed_MotoG4_9.png');
prog(:,:,:,20) = imread('chart_processed_MotoG4_10.png');

cal_img = zeros(sz);
cal_img2 = zeros(sz);
cal_img3 = zeros(sz);
proc_mean = zeros([24 4 20]);
cal_mean = zeros([24 3 20]);
a = zeros([4 3 n]);
patch_info = load('patch_info.mat');
patch_info = patch_info.patch_info;
%[cal_img1, a1, fit, proc_mean1, cal_mean(:,:,2), orig_mean] = color_xform(orig, 1, 1, 476, 320, patch_info.patch_info, prog(:,:,:,2), 1, 1, 476, 320, 0, 0);

for i = 1:n;
    [cal_img(:,:,:,i), a(:,:,i), fit, proc_mean(:,:,i), cal_mean(:,:,i), orig_mean] = color_xform(orig, 1, 1, 476, 320, patch_info, prog(:,:,:,i), 1, 1, 476, 320, 0, 0);
    close all;
end

a_samsung = a(:,:,1:10);
a_moto = a(:,:,11:20);

a_avg_moto = mean(a_moto,3);
a_avg_samsung = mean(a_samsung,3);

cal_mean2 = zeros([24 3 20]);
for i = 1:10;
    cal_img_raw = double([ones(nr*nc,1) reshape(permute(prog(:,:,:,i),[3,1,2]), 3, nr*nc)'])*a_avg_samsung;
    cal_img2(:,:,:,i) = reshape(cal_img_raw, nr, nc,3);
    cal_img2(:,:,:,i) = refining_checker_generator(320, 476, cal_img2(:,:,:,i), a_avg_samsung(1,:));    
    for j = 1:24;
        % Processed image
        cr_1 = patch_info(j,1); % top coordinate of processed patch
        cc_1 = patch_info(j,2); % left coordinate of processed patch
        cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
        cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
        cal_mean2(j,:,i) = ...
             mean(reshape(cal_img2(cr_1:cr_2, cc_1:cc_2, :,i),patch_info(j,3)*patch_info(j,4),3));
    end
    cal_img(:,:,:,i) = refining_checker_generator(320, 476, cal_img(:,:,:,i), a(1,:,i));
end

for i = 11:20;
    cal_img_raw = double([ones(nr*nc,1) reshape(permute(prog(:,:,:,i),[3,1,2]), 3, nr*nc)'])*a_avg_moto;
    cal_img2(:,:,:,i) = reshape(cal_img_raw, nr, nc,3);
    cal_img2(:,:,:,i) = refining_checker_generator(320, 476, cal_img2(:,:,:,i), a_avg_moto(1,:));    
    for j = 1:24;
        % Processed image
        cr_1 = patch_info(j,1); % top coordinate of processed patch
        cc_1 = patch_info(j,2); % left coordinate of processed patch
        cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
        cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
        cal_mean2(j,:,i) = ...
             mean(reshape(cal_img2(cr_1:cr_2, cc_1:cc_2, :,i),patch_info(j,3)*patch_info(j,4),3));
    end
    cal_img(:,:,:,i) = refining_checker_generator(320, 476, cal_img(:,:,:,i), a(1,:,i));
end

red_respond   = zeros([24 5 20]);
green_respond = zeros([24 5 20]);
blue_respond  = zeros([24 5 20]);

cal_img_samsung = cal_img(:,:,:,1:10);
cal_img_samsung_avg = mean(cal_img_samsung,4);
cal_img_moto = cal_img(:,:,:,11:20);
cal_img_moto_avg = mean(cal_img_moto,4);
for j = 1:24;
   % Processed image
   cr_1 = patch_info(j,1); % top coordinate of processed patch
   cc_1 = patch_info(j,2); % left coordinate of processed patch
   cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
   cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
   cal_img_samsung_avg_mean(j,:) = mean(reshape(cal_img_samsung_avg(cr_1:cr_2, cc_1:cc_2, :),patch_info(j,3)*patch_info(j,4),3));
   cal_img_moto_avg_mean(j,:) = mean(reshape(cal_img_moto_avg(cr_1:cr_2, cc_1:cc_2, :),patch_info(j,3)*patch_info(j,4),3));
end

for i = 1:10
    red_respond(:,:,i)   = [orig_mean(:,1) cal_mean(:,1,i) proc_mean(:,2,i) cal_mean2(:,1,i) cal_img_samsung_avg_mean(:,1)];
    red_respond(:,:,i)   = sortrows(red_respond(:,:,i),1);
    green_respond(:,:,i) = [orig_mean(:,2) cal_mean(:,2,i) proc_mean(:,3,i) cal_mean2(:,2,i) cal_img_samsung_avg_mean(:,2)];
    green_respond(:,:,i) = sortrows(green_respond(:,:,i),1);
    blue_respond(:,:,i)  = [orig_mean(:,3) cal_mean(:,3,i) proc_mean(:,4,i) cal_mean2(:,3,i) cal_img_samsung_avg_mean(:,3)];
    blue_respond(:,:,i)  = sortrows(blue_respond(:,:,i),1);
end

for i = 11:20
    red_respond(:,:,i)   = [orig_mean(:,1) cal_mean(:,1,i) proc_mean(:,2,i) cal_mean2(:,1,i) cal_img_moto_avg_mean(:,1)];
    red_respond(:,:,i)   = sortrows(red_respond(:,:,i),1);
    green_respond(:,:,i) = [orig_mean(:,2) cal_mean(:,2,i) proc_mean(:,3,i) cal_mean2(:,2,i) cal_img_moto_avg_mean(:,2)];
    green_respond(:,:,i) = sortrows(green_respond(:,:,i),1);
    blue_respond(:,:,i)  = [orig_mean(:,3) cal_mean(:,3,i) proc_mean(:,4,i) cal_mean2(:,3,i) cal_img_moto_avg_mean(:,3)];
    blue_respond(:,:,i)  = sortrows(blue_respond(:,:,i),1);
end

figure
hold on
plot(red_respond(:,1,1),'--');
plot(red_respond(:,5,1));
plot(red_respond(:,5,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Red channel, averaging 10 calibrated image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(green_respond(:,1,1),'--');
plot(green_respond(:,5,1));
plot(green_respond(:,5,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Green channel, averaging 10 calibrated image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(blue_respond(:,1,1),'--');
plot(blue_respond(:,5,1));
plot(blue_respond(:,5,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Blue channel, averaging 10 calibrated image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(red_respond(:,1,1),'--');
plot(red_respond(:,3,1));
plot(red_respond(:,3,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Red channel, uncorrected image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(green_respond(:,1,1),'--');
plot(green_respond(:,3,1));
plot(green_respond(:,3,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Green channel, uncorrected image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(blue_respond(:,1,1),'--');
plot(blue_respond(:,3,1));
plot(blue_respond(:,3,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Blue channel, uncorrected image');
ylabel('Color value') 
xlabel('Color patches') 
hold off


figure
hold on
plot(red_respond(:,1,1));
plot(red_respond(:,2,1));
plot(red_respond(:,3,1));
plot(red_respond(:,4,1));
plot(red_respond(:,5,1));
hold off

figure
hold on
plot(green_respond(:,1,1), green_respond(:,2,1));
plot(green_respond(:,1,1), green_respond(:,3,1));
plot(green_respond(:,1,1), green_respond(:,5,1),'--');
hold off

figure
hold on
plot(blue_respond(:,1,1), blue_respond(:,2,1));
plot(blue_respond(:,1,1), blue_respond(:,3,1));
plot(blue_respond(:,1,1), blue_respond(:,4,1),'--');
hold off

figure
hold on
plot(red_respond(:,1,1), red_respond(:,2,1));
plot(red_respond(:,1,1), red_respond(:,3,1));
plot(red_respond(:,1,1), red_respond(:,5,1),'--');
hold off

figure
hold on
plot(green_respond(:,1,1), green_respond(:,2,1));
plot(green_respond(:,1,1), green_respond(:,3,1));
plot(green_respond(:,1,1), green_respond(:,5,1),'--');
hold off

figure
hold on
plot(blue_respond(:,1,1), blue_respond(:,2,1));
plot(blue_respond(:,1,1), blue_respond(:,3,1));
plot(blue_respond(:,1,1), blue_respond(:,4,1),'--');
hold off

figure
image(cal_img2(:,:,:,1)/255);
figure
image(cal_img_avg/255);
figure
image(cal_img(:,:,:,1)/255);