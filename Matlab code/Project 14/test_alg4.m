clc; close all; clear all;
n=9;
nr = 320;
nc = 476;
orig = imread('chart_classic.png');
sz = [size(orig) 9];
prog = zeros(sz);

prog(:,:,:,1) = imread('chart_processed_m_1.png');
prog(:,:,:,2) = imread('chart_processed_m_2.png');
prog(:,:,:,3) = imread('chart_processed_m_3.png');
prog(:,:,:,4) = imread('chart_processed_m_4.png');
prog(:,:,:,5) = imread('chart_processed_m_5.png');
prog(:,:,:,6) = imread('chart_processed_m_6.png');
prog(:,:,:,7) = imread('chart_processed_m_7.png');
prog(:,:,:,8) = imread('chart_processed_m_8.png');
prog(:,:,:,9) = imread('chart_processed_m_9.png');

cal_img = zeros(sz);
cal_img2 = zeros(sz);
cal_img3 = zeros(sz);
proc_mean = zeros([24 4 9]);
cal_mean = zeros([24 3 9]);
a = zeros([4 3 n]);
patch_info = load('patch_info.mat');
patch_info = patch_info.patch_info;
%[cal_img1, a1, fit, proc_mean1, cal_mean(:,:,2), orig_mean] = color_xform(orig, 1, 1, 476, 320, patch_info.patch_info, prog(:,:,:,2), 1, 1, 476, 320, 0, 0);

for i = 1:n;
    [cal_img(:,:,:,i), a(:,:,i), fit, proc_mean(:,:,i), cal_mean(:,:,i), orig_mean] = color_xform(orig, 1, 1, 476, 320, patch_info, prog(:,:,:,i), 1, 1, 476, 320, 0, 0);
    close all;
end

a_moto = a(:,:,1:9);
a_avg_moto = mean(a_moto,3);
cal_mean2 = zeros([24 3 9]);

for i = 1:9;
    cal_img_raw = double([ones(nr*nc,1) reshape(permute(prog(:,:,:,i),[3,1,2]), 3, nr*nc)'])*a_avg_moto;
    cal_img2(:,:,:,i) = reshape(cal_img_raw, nr, nc,3);
    cal_img2(:,:,:,i) = refining_checker_generator(320, 476, cal_img2(:,:,:,i), a_avg_moto(1,:));    
    for j = 1:24;
        % Processed image
        cr_1 = patch_info(j,1); % top coordinate of processed patch
        cc_1 = patch_info(j,2); % left coordinate of processed patch
        cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
        cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
        cal_mean2(j,:,i) = ...
             mean(reshape(cal_img2(cr_1:cr_2, cc_1:cc_2, :,i),patch_info(j,3)*patch_info(j,4),3));
    end
    cal_img(:,:,:,i) = refining_checker_generator(320, 476, cal_img(:,:,:,i), a(1,:,i));
end

red_respond   = zeros([24 5 9]);
green_respond = zeros([24 5 9]);
blue_respond  = zeros([24 5 9]);

cal_img_moto = cal_img(:,:,:,1:9);
cal_img_moto_avg = mean(cal_img_moto,4);
for j = 1:24;
   % Processed image
   cr_1 = patch_info(j,1); % top coordinate of processed patch
   cc_1 = patch_info(j,2); % left coordinate of processed patch
   cr_2 = patch_info(j,1) + patch_info(j,3) - 1; % bottom coordinate of processed patch
   cc_2 = patch_info(j,2) + patch_info(j,4) - 1; % right coordinate of processed patch
   cal_img_moto_avg_mean(j,:) = mean(reshape(cal_img_moto_avg(cr_1:cr_2, cc_1:cc_2, :),patch_info(j,3)*patch_info(j,4),3));
end

for i = 1:9
    red_respond(:,:,i)   = [orig_mean(:,1) cal_mean(:,1,i) proc_mean(:,2,i) cal_mean2(:,1,i) cal_img_moto_avg_mean(:,1)];
    red_respond(:,:,i)   = sortrows(red_respond(:,:,i),1);
    green_respond(:,:,i) = [orig_mean(:,2) cal_mean(:,2,i) proc_mean(:,3,i) cal_mean2(:,2,i) cal_img_moto_avg_mean(:,2)];
    green_respond(:,:,i) = sortrows(green_respond(:,:,i),1);
    blue_respond(:,:,i)  = [orig_mean(:,3) cal_mean(:,3,i) proc_mean(:,4,i) cal_mean2(:,3,i) cal_img_moto_avg_mean(:,3)];
    blue_respond(:,:,i)  = sortrows(blue_respond(:,:,i),1);
end

figure
hold on
plot(red_respond(:,1,1), red_respond(:,1,1),'--');
plot(red_respond(:,1,1), red_respond(:,5,1));
%plot(red_respond(:,2,1));
legend('');
title('Red channel, averaging 9 calibrated image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

avg_moto_R = sum(sum(cal_img1(:,:,1))/(70*70*24));
avg_moto_G = sum(sum(cal_img1(:,:,2))/(70*70*24));
avg_moto_B = sum(sum(cal_img1(:,:,3))/(70*70*24));
avg_moto = [avg_moto_R avg_moto_G avg_moto_B];

avg_prog_moto_R = sum(sum(prog1(:,:,1))/(70*70*24));
avg_prog_moto_G = sum(sum(prog1(:,:,2))/(70*70*24));
avg_prog_moto_B = sum(sum(prog1(:,:,3))/(70*70*24));
avg_prog_moto = [avg_prog_moto_R avg_prog_moto_G avg_prog_moto_B];


figure
hold on
plot(green_respond(:,1,1),'--');
plot(green_respond(:,5,1));
plot(green_respond(:,5,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Green channel, averaging 10 calibrated image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(blue_respond(:,1,1),'--');
plot(blue_respond(:,5,1));
plot(blue_respond(:,5,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Blue channel, averaging 10 calibrated image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(red_respond(:,1,1),'--');
plot(red_respond(:,3,1));
plot(red_respond(:,3,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Red channel, uncorrected image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(green_respond(:,1,1),'--');
plot(green_respond(:,3,1));
plot(green_respond(:,3,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Green channel, uncorrected image');
ylabel('Color value') 
xlabel('Color patches') 
hold off

figure
hold on
plot(blue_respond(:,1,1),'--');
plot(blue_respond(:,3,1));
plot(blue_respond(:,3,11));
legend('Original', 'Samsung Phone', 'Motorola Phone');
title('Blue channel, uncorrected image');
ylabel('Color value') 
xlabel('Color patches') 
hold off


figure
hold on
plot(red_respond(:,1,1));
plot(red_respond(:,2,1));
plot(red_respond(:,3,1));
plot(red_respond(:,4,1));
plot(red_respond(:,5,1));
hold off

figure
hold on
plot(green_respond(:,1,1), green_respond(:,2,1));
plot(green_respond(:,1,1), green_respond(:,3,1));
plot(green_respond(:,1,1), green_respond(:,5,1),'--');
hold off

figure
hold on
plot(blue_respond(:,1,1), blue_respond(:,2,1));
plot(blue_respond(:,1,1), blue_respond(:,3,1));
plot(blue_respond(:,1,1), blue_respond(:,4,1),'--');
hold off

figure
hold on
plot(red_respond(:,1,1), red_respond(:,2,1));
plot(red_respond(:,1,1), red_respond(:,3,1));
plot(red_respond(:,1,1), red_respond(:,5,1),'--');
hold off

figure
hold on
plot(green_respond(:,1,1), green_respond(:,2,1));
plot(green_respond(:,1,1), green_respond(:,3,1));
plot(green_respond(:,1,1), green_respond(:,5,1),'--');
hold off

figure
hold on
plot(blue_respond(:,1,1), blue_respond(:,2,1));
plot(blue_respond(:,1,1), blue_respond(:,3,1));
plot(blue_respond(:,1,1), blue_respond(:,4,1),'--');
hold off

figure
image(cal_img2(:,:,:,1)/255);
figure
image(cal_img_avg/255);
figure
image(cal_img(:,:,:,1)/255);