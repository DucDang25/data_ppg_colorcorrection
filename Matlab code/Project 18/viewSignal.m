clc; clear all; close all;

fileIDMoto = fopen('/Data/data_final_moto_1.txt','r');
fileIDSamsung = fopen('/Data/data_final_sam_1.txt','r');

fileIDMotoCorrected = fopen('/Data/data_final_corrected_moto_1.txt','r');
fileIDSamsungCorrected = fopen('/Data/data_final_corrected_sam_1.txt','r');

formatSpec = '%f';

data_moto = fscanf(fileIDMoto,formatSpec);
data_sam = fscanf(fileIDSamsung,formatSpec);
data_moto_corrected = fscanf(fileIDMotoCorrected,formatSpec);
data_sam_corrected = fscanf(fileIDSamsungCorrected,formatSpec);

fclose(fileIDMoto);
fclose(fileIDSamsung);

fclose(fileIDMotoCorrected);
fclose(fileIDSamsungCorrected);

figure;
plot(data_moto);

figure;
plot(data_sam);

figure;
plot(data_moto_corrected);

figure;
plot(data_sam_corrected);