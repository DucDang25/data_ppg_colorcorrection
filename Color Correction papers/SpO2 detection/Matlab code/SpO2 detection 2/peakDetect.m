function [ maxima, minima ] = peakDetect( signal, MinPeakHeight, MinPeakDistance )
    [~,maxima] = findpeaks(signal,'MinPeakHeight',MinPeakHeight,'MinPeakDistance',MinPeakDistance);
    signal_inv = -signal;
    [~,minima] = findpeaks(signal_inv,'MinPeakHeight',MinPeakHeight,'MinPeakDistance',MinPeakDistance);
end

