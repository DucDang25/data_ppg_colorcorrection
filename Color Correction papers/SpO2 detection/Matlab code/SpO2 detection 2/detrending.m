function [ smoothData ] = detrending( signal )
% Detrending Data
    [p,s,mu] = polyfit((1:numel(signal))',signal,3);
    p
    f_y = polyval(p,(1:numel(signal))',[],mu);
    smoothData = signal - f_y;        % Detrend data
    figure;
    plot(f_y);

   % smoothData = sgolayfilt(smoothData,7,21);
end

