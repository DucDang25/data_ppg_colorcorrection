clc; close all; clear all
fileID = fopen('data_ex_-12.txt','r');
formatSpec = '%f';
PPGrawFull = fscanf(fileID,formatSpec);

PPGraw = PPGrawFull(200:300);



%% Measuring Amplitudes of Peaks
t = 1:length(PPGraw);

figure
plot(t,PPGraw)
title('Signal with a Trend')
xlabel('Samples');
ylabel('Voltage(mV)')
legend('Noisy PPG Signal')
grid on

%% Detrending Data
[p,s,mu] = polyfit((1:numel(PPGraw))',PPGraw,6);
f_y = polyval(p,(1:numel(PPGraw))',[],mu);

PPG_data = PPGraw - f_y;        % Detrend data

figure
plot(t,PPG_data)
grid on
ax = axis;
axis([ax(1:2) -1.2 1.2])
title('Detrended PPG Signal')
xlabel('Samples')
ylabel('Voltage(mV)')
legend('Detrended PPG Signal')




%% Thresholding to Find Peaks of Interest
[~,locs_Rwave] = findpeaks(PPG_data,'MinPeakHeight',0.15,'MinPeakDistance',15);

%% Finding Local Minima in Signal
PPG_inverted = -PPG_data;
[~,locs_Swave] = findpeaks(PPG_inverted,'MinPeakHeight',0.15,'MinPeakDistance',5);

%% The following plot shows the R-waves and S-waves detected in the signal.
figure
hold on
plot(t,PPG_data)
plot(locs_Rwave,PPG_data(locs_Rwave),'rv','MarkerFaceColor','r')
plot(locs_Swave,PPG_data(locs_Swave),'rs','MarkerFaceColor','b')
axis([0 500 -1.1 1.1])
grid on
legend('PPG Signal','R-waves','S-waves')
xlabel('Samples')
ylabel('Voltage(mV)')
title('R-wave and S-wave in Noisy ECG Signal')

%% determine the locations of the Q-waves
smoothPPG = sgolayfilt(PPG_data,7,21);

figure
plot(t,PPG_data,'b',t,smoothPPG,'r')
grid on
axis tight
xlabel('Samples')
ylabel('Voltage(mV)')
legend('Noisy PPG Signal','Filtered Signal')
title('Filtering Noisy PPG Signal')

[~,min_locs] = findpeaks(-smoothPPG,'MinPeakDistance',40);

% Peaks between -0.2mV and -0.5mV
locs_Qwave = min_locs(smoothPPG(min_locs)>-0.5 & smoothPPG(min_locs)<-0.2);

figure
hold on
plot(t,smoothPPG);
plot(locs_Qwave,smoothPPG(locs_Qwave),'rs','MarkerFaceColor','g')
plot(locs_Rwave,smoothPPG(locs_Rwave),'rv','MarkerFaceColor','r')
plot(locs_Swave,smoothPPG(locs_Swave),'rs','MarkerFaceColor','b')
grid on
title('Thresholding Peaks in Signal')
xlabel('Samples')
ylabel('Voltage(mV)')
ax = axis;
axis([0 1850 -1.1 1.1])
legend('Smooth PPG signal','Q-wave','R-wave','S-wave')